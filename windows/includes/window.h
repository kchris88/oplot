#pragma once

// System includes
#include <thread>

// Cairo includes
#define __CAIRO
#ifdef __CAIRO
#include <cairo/cairo.h>
#endif

// Oplot includes
#include <size.h>

namespace oplot {

class Window {
public:
  Window(const std::string &title = std::string(), const Size &display_size = Size());
  Window(const Window &other);
  ~Window() = default;

  virtual void Show(cairo_surface_t **surface) = 0;
  virtual void Wait() = 0;
  Size &DisplaySize();
  Size DisplaySize() const;
  Size ScreenSize() const;
  virtual std::string GetTitle() const;
  virtual void SetTitle(const std::string &title);
  void SetDelay(int delay_ms);
  virtual void Release() = 0;

protected:
  virtual Size GetScreenResolution() = 0;
  virtual void DefaultDisplaySize() = 0;
  void SetScale(cairo_surface_t **surface);

  std::string _title;
  Size _display_size;
  Size _screen_size;
  Size _scaled_size;
  double _scale;
  bool _is_scaled;
  int _delay_ms;
  std::unique_ptr<std::thread> _window_thread_ptr;
};

} //  namespace oplot