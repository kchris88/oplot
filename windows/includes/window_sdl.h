#pragma once

// SDL includes
#include <SDL2/SDL.h>

// Oplot includes
#include <window.h>

namespace oplot {

class WindowSDL : public Window {
public:
  WindowSDL(const std::string &title = std::string(), const Size &display_size = Size());
  WindowSDL(const WindowSDL &other);
  ~WindowSDL();

  void Show(cairo_surface_t **surface) override;
  void Wait() override;
  void Release() override;

  void SetTitle(const std::string &title) override;

protected:
  void PrepareWindowAndSurface(cairo_surface_t **surface);
  void WindowInit();
  void WindowResize();
  void ClearImage();
  Size GetScreenResolution() override;
  void DefaultDisplaySize() override;

  SDL_Window *_window;
  SDL_Surface *_window_surface;
  cairo_surface_t *_image_surface;
  cairo_t *_image_ctx;
};

} //  namespace oplot