#include <window_sdl.h>

// System includes
#include <iostream>

namespace oplot {

WindowSDL::WindowSDL(const std::string &title, const Size &display_size)
  : Window(title, display_size),
    _window(nullptr),
    _window_surface(nullptr),
    _image_surface(nullptr),
    _image_ctx(nullptr) {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    throw("Error: SDL cannot be initialized");
  }
  _screen_size = GetScreenResolution();
  if (_display_size.width == 0  || 
      _display_size.height == 0 ||
      _display_size.width > _screen_size.width ||
      _display_size.height > _screen_size.height) {
    DefaultDisplaySize();
  }
  WindowInit();
}

WindowSDL::WindowSDL(const WindowSDL &other)
  : Window(other),
    _window(other._window),
    _window_surface(other._window_surface) {
  WindowInit();
}

WindowSDL::~WindowSDL() {
  Release();
}

void WindowSDL::Show(cairo_surface_t **surface) {
  PrepareWindowAndSurface(surface);
  SDL_ShowWindow(_window);
  SDL_UpdateWindowSurface(_window);
  Wait();
}

void WindowSDL::Wait() {
  if (_delay_ms > 0) {
    SDL_Delay(_delay_ms);
  }
}

void WindowSDL::Release() {
  ClearImage();
  SDL_DestroyWindow(_window);
}

void WindowSDL::SetTitle(const std::string &title) {
  _title = title;
  SDL_SetWindowTitle(_window, _title.c_str());
}

void WindowSDL::PrepareWindowAndSurface(cairo_surface_t **surface) {
  SetScale(surface);
  Size display_size;
  display_size.width = cairo_image_surface_get_width(*surface) * _scale;
  display_size.height = cairo_image_surface_get_height(*surface) * _scale;
  if (display_size != _display_size) {
    _display_size = display_size;
    WindowResize();
    _is_scaled = false;
  }
  if (!_is_scaled) {
    cairo_scale(_image_ctx, _scale, _scale); 
    _is_scaled = true;
  }
  cairo_set_source_surface(_image_ctx, *surface, 0.0, 0.0);
  cairo_paint(_image_ctx);
}

void WindowSDL::WindowInit() {
  SDL_Init(SDL_INIT_VIDEO);
  _window = SDL_CreateWindow(_title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
                             _display_size.width, _display_size.height, SDL_WINDOW_HIDDEN);
  _window_surface = SDL_GetWindowSurface(_window);
  unsigned char* data = (unsigned char*)_window_surface->pixels;
  _image_surface = cairo_image_surface_create_for_data(data, CAIRO_FORMAT_ARGB32, 
                                                       _display_size.width, _display_size.height,
                                                       _window_surface->pitch);
  _image_ctx = cairo_create(_image_surface);
}

void WindowSDL::WindowResize() {
  ClearImage();
  SDL_SetWindowSize(_window, _display_size.width, _display_size.height);
  _window_surface = SDL_GetWindowSurface(_window);
  unsigned char* data = (unsigned char*)_window_surface->pixels;
  _image_surface = cairo_image_surface_create_for_data(data, CAIRO_FORMAT_ARGB32, 
                                                       _display_size.width, _display_size.height,
                                                       _window_surface->pitch);
  _image_ctx = cairo_create(_image_surface);
}

void WindowSDL::ClearImage() {
  if (_image_ctx) {
    cairo_destroy(_image_ctx);
  }
  if (_image_surface) {
    cairo_surface_destroy(_image_surface);
  }
}

Size WindowSDL::GetScreenResolution() {
  SDL_DisplayMode display_mode;
  SDL_GetCurrentDisplayMode(0, &display_mode);
  if (SDL_GetDesktopDisplayMode(0, &display_mode) != 0) {
    std::cout << SDL_GetError() << std::endl;
  }
  return Size(display_mode.w, display_mode.h);
}

void WindowSDL::DefaultDisplaySize() {
  if (_screen_size.width > _screen_size.height) {
    _display_size.height = static_cast<int>(_screen_size.height * 0.9);
    _display_size.width = _display_size.height;
  } else {
    _display_size.width = static_cast<int>(_screen_size.width * 0.9);
    _display_size.height = _display_size.width;
  }
}

} //  namespace oplot