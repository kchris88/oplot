#include <window.h>

namespace oplot {

Window::Window(const std::string &title, const Size &display_size)
  : _title(title),
    _display_size(display_size),
    _screen_size(Size()),
    _scaled_size(Size()),
    _scale(0.0),
    _is_scaled(false),
    _delay_ms(0) {

}

Window::Window(const Window &other)
  : _title(other._title),
    _display_size(other.DisplaySize()),
    _screen_size(other.ScreenSize()),
    _scaled_size(other._scaled_size),
    _scale(other._scale),
    _is_scaled(other._is_scaled),
    _delay_ms(other._delay_ms) {
  
}

Size &Window::DisplaySize() {
  return _display_size;
}

Size Window::DisplaySize() const {
  return _display_size;
}

Size Window::ScreenSize() const {
  return _screen_size;
}

std::string Window::GetTitle() const {
  return _title;
}

void Window::SetTitle(const std::string &title) {
  _title = title;
}

void Window::SetDelay(int delay_ms) {
  _delay_ms = delay_ms;
}

void Window::SetScale(cairo_surface_t **surface) {
  double display_width = static_cast<double>(_display_size.width);
  double display_height = static_cast<double>(_display_size.height);
  if (display_width < display_height) {
    _scale = display_height / static_cast<double>(cairo_image_surface_get_height(*surface));
  } else {
    _scale = display_width / static_cast<double>(cairo_image_surface_get_width(*surface));
  }
}

} //  namespace oplot