#include <iostream>
#include <oplot/oplot.h>

int main() {
  // PLOT
  std::vector<double> data_1 = {0.0, 1000.0, 2000.0, 3000.0};
  std::vector<double> data_2 = {0.0, 1000.0, 1200.0, 2000.0, 300.0};
  std::vector<double> data_3 = {0.0, 1500.0, 700.0, 1300.0};
  oplot::Range<double> range(0.0, 6.0, -100.0, 3500.0);
  oplot::Size size(1000, 1000);
  oplot::Plot2D plot(size);
  plot.settings->grid_on = true;
  plot.settings->SetRange(range);
  plot.settings->shift = oplot::Step<int>(1, 0);
  plot.settings->x_key_values = false;
  plot.settings->y_key_values = true;
  plot.settings->label_x = "X data";
  plot.settings->label_y = "Y data";
  plot.settings->measure_points_on = true;
  plot.settings->plot_lines_on = true;
  plot.legend().Push("Data 2");
  plot.legend().Push("Data 3");
  plot.legend().Push("Data 1");
  plot.settings->title = "Plot2D";
  plot.Draw(data_2);
  plot.Show(2000);
  plot.Draw(data_3);
  plot.Show(500);
  plot.Draw(data_1);
  plot.Show(2000);
  plot.Save("plot2d");
  
  return 0;
}