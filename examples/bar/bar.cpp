#include <iostream>
#include <oplot/oplot.h>

int main() {
  // BAR
  std::vector<std::vector<double>> data;
  data.push_back({1.0, 0.9});
  data.push_back({5.0, 3.7});
  data.push_back({4.0, -4.5});
  data.push_back({2.0, 3.9});

  std::vector<std::string> legend;
  legend.push_back("data 1");
  legend.push_back("data 2");
  legend.push_back("data 3");
  legend.push_back("data 4");

  std::vector<std::string> legend2;
  legend2.push_back("version 1");
  legend2.push_back("version 2");

  oplot::Bar bar;
  bar.settings->label_x = "Classes";
  bar.settings->label_y = "Values";
  bar.settings->grid_on = true;
  bar.settings->axis_step.Set(12, 12);
  bar.settings->key_values = false;
  bar.settings->SetRange(oplot::Range<double>(0.0, 0.0, -6.0, 6.0));
  bar.legend().Push(legend2);
  bar.Draw(data, legend);
  bar.Show(2000);
  bar.Save("bar");
  
  return 0;
}