#include <iostream>
#include <oplot/oplot.h>

int main() {
  // PIE
  std::vector<double> data;
  data.push_back(17.0);
  data.push_back(33.0);
  data.push_back(33.0);
  data.push_back(17.0);

  std::vector<std::string> legend;
  legend.push_back("data 1");
  legend.push_back("data 2");
  legend.push_back("data 3");
  legend.push_back("data 4");

  oplot::Pie pie;
  pie.legend().Push(legend);
  pie.Draw(data);
  pie.Show(2000);
  pie.Save("pie1");

  pie.settings->rotation = -M_PI_2;
  pie.settings->explode = {true, false, false, false};
  pie.Draw(data);
  pie.Show(2000);
  pie.Save("pie2");
  
  return 0;
}