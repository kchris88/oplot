#include <iostream>
#include <oplot/oplot.h>

int main() {
  // HIST
  std::vector<double> data;
  data.reserve(1000);
  std::srand(std::time(NULL));
  for (size_t i = 0; i < 1000; ++i) {
    data.emplace_back(std::rand()%21);
  }
  oplot::Hist hist;
  oplot::Range<double> range_hist(0.0, 20.0, 0.0, 100.0);
  hist.settings->SetRange(range_hist);
  hist.settings->axis_step.Set(10, 10);
  hist.settings->bins = 20;
  hist.settings->key_values = false;
  hist.settings->grid_on = true;
  hist.Draw(data);
  hist.Show(1000);
  hist.Save("hist");
  
  return 0;
}