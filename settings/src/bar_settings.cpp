#include <bar_settings.h>

namespace oplot {

BarSettings::BarSettings()
  : Settings(),
    axis_step(Step<int>(9, 9)),
    shift(Step<int>(0, 0)),
    grid_on(false),
    single_color(false),
    key_values(false),
    label_x(""),
    label_y(""),
    _range(Range<double>()),
    _is_range_set(false) {

}

void BarSettings::Clear() {
  title = "";
  _range.Clear();
  _is_range_set = false;
  axis_step.Set(9, 9);
  shift.Set(0, 0);
  grid_on = false;
  single_color = false;
  key_values = false;
  label_x = "";
  label_y = "";
}

Range<double> BarSettings::GetRange() const {
  return _range;
}

void BarSettings::SetRange(const Range<double> &range, bool autorange) {
  _range = range;
  _is_range_set = !autorange;
}

bool BarSettings::IsRangeSet() const {
  return _is_range_set;
}

} //  namespace oplot