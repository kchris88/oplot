#include <pie_settings.h>

namespace oplot {

PieSettings::PieSettings()
  : Settings(),
    rotation(0.0) {

}

void PieSettings::Clear() {
  title = "";
  rotation = 0.0;
  explode.clear();
}

} //  namespace oplot