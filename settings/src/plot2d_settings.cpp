#include <plot2d_settings.h>

namespace oplot {

Plot2DSettings::Plot2DSettings() 
  : Settings(),
    axis_step(Step<int>(9, 9)),
    shift(Step<int>(0, 0)),
    grid_on(false),
    x_key_values(false),
    y_key_values(false),
    measure_points_on(false),
    plot_lines_on(true),
    label_x(""),
    label_y(""),
    _range(Range<double>()),
    _is_range_set(false) {
  
}

void Plot2DSettings::Clear() {
  title = "";
  _range.Clear();
  _is_range_set = false;
  axis_step.Set(9, 9);
  shift.Set(0, 0);
  grid_on = false;
  x_key_values = false;
  y_key_values = false;
  measure_points_on = false;
  plot_lines_on = true;
  label_x = "";
  label_y = "";
}

Range<double> Plot2DSettings::GetRange() const {
  return _range;
}

void Plot2DSettings::SetRange(const Range<double> &range, bool autorange) {
  _range = range;
  _is_range_set = !autorange;
}

bool Plot2DSettings::IsRangeSet() const {
  return _is_range_set;
}

} //  namespace oplot