#include <settings.h>

namespace oplot {

Settings::Settings() : title("") { }

void Settings::Clear() {
  title = "";
}

} //  namespace oplot