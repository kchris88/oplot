#include <hist_settings.h>

namespace oplot {

HistSettings::HistSettings()
  : Settings(),
    axis_step(Step<int>(9, 9)),
    shift(Step<int>(0, 0)),
    bins(10),
    grid_on(false),
    key_values(false),
    label_x(""),
    label_y(""),
    _range(Range<double>()),
    _is_range_set(false) {

}

void HistSettings::Clear() {
  title = "";
  _range.Clear();
  _is_range_set = false;
  axis_step.Set(9, 9);
  shift.Set(0, 0);
  bins = 10;
  grid_on = false;
  key_values = false;
  label_x = "";
  label_y = "";
}

Range<double> HistSettings::GetRange() const {
  return _range;
}

void HistSettings::SetRange(const Range<double> &range, bool autorange) {
  _range = range;
  _is_range_set = !autorange;
}

bool HistSettings::IsRangeSet() const {
  return _is_range_set;
}

} //  namespace oplot