#pragma once

// System includes
#include <string>

namespace oplot {

class Settings {
public:
  Settings();

  Settings(const Settings &other) = default;
  ~Settings() = default;

  virtual void Clear();

  std::string title;
};

} //  namespace oplot