#pragma once

// Oplot includes
#include <range.h>
#include <settings.h>
#include <step.h>

namespace oplot {

class HistSettings : public Settings {
public:
  HistSettings();

  HistSettings(const HistSettings &other) = default;
  ~HistSettings() = default;

  virtual void Clear() override;

  Range<double> GetRange() const;

  void SetRange(const Range<double> &range, bool autorange = false);

  bool IsRangeSet() const;

  Step<int> axis_step;
  Step<int> shift;
  size_t bins;
  bool grid_on;
  bool key_values;
  std::string label_x;
  std::string label_y;

protected:
  Range<double> _range;
  bool _is_range_set;
};

} //  namespace oplot