#pragma once

// Oplot includes
#include <range.h>
#include <settings.h>
#include <step.h>

namespace oplot {

class Plot2DSettings : public Settings {
public:
  Plot2DSettings();

  Plot2DSettings(const Plot2DSettings &other) = default;
  ~Plot2DSettings() = default;

  virtual void Clear() override;

  Range<double> GetRange() const;

  void SetRange(const Range<double> &range, bool autorange = false);

  bool IsRangeSet() const;

  Step<int> axis_step;
  Step<int> shift;
  bool grid_on;
  bool x_key_values;
  bool y_key_values;
  bool measure_points_on;
  bool plot_lines_on;
  std::string label_x;
  std::string label_y;

protected:
  Range<double> _range;
  bool _is_range_set;
};

} //  namespace oplot