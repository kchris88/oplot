#pragma once

// System includes
#include <vector>

// Oplot includes
#include <range.h>
#include <settings.h>
#include <step.h>

namespace oplot {

class PieSettings : public Settings {
public:
  PieSettings();

  PieSettings(const PieSettings &other) = default;
  ~PieSettings() = default;

  virtual void Clear() override;

  double rotation;
  std::vector<bool> explode;
};

} //  namespace oplot