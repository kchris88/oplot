#!/bin/bash

sudo apt-cache search libsdl2
sudo apt install libcairo2-dev libsdl2-dev cmake

mkdir build
cd build
cmake ..
sudo make install

sudo /bin/bash -c 'echo "/usr/local/lib/oplot" > /etc/ld.so.conf.d/oplot.conf'
sudo ldconfig