#pragma once

namespace oplot {

template <class T>
class Point2D {
public:
  Point2D() = default;
  Point2D(const T x, const T y);
  Point2D(const Point2D &other) = default;
  ~Point2D() = default;

  void Set(const T x, const T y);
  virtual void Clear();

  Point2D<T> &operator=(const Point2D<T> &point);

  T x;
  T y;
};

template <class T>
Point2D<T>::Point2D(const T x, const T y)
  : x(x),
    y(y) {

}

template <class T>
void Point2D<T>::Set(const T x, const T y) {
  this->x = x;
  this->y = y;
}

template <class T>
void Point2D<T>::Clear() {
  x = static_cast<T>(0);
  y = static_cast<T>(0);
}

template <class T>
Point2D<T> &Point2D<T>::operator=(const Point2D<T> &point) {
  x = point.x;
  y = point.y;
  
  return *this;
}

} // namespace oplot