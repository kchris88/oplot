#pragma once

// Cairo includes
#ifdef __CAIRO
#include <cairo/cairo.h>
#endif

namespace oplot {

struct ContextGuard {
public:
  ContextGuard() = delete;
  ContextGuard(cairo_surface_t *surface, cairo_t **context) {
    *context = cairo_create(surface);
    _ctx = *context;
  }
  ~ContextGuard() {
    cairo_destroy(_ctx);
  }
private:
  cairo_t *_ctx;
};

} //  namespace oplot