#pragma once

namespace oplot {

class Rectangle {
public:
  Rectangle();
  Rectangle(const int x, const int y, const int width, const int height);
  Rectangle(const Rectangle &other) = default;
  ~Rectangle() = default;

  void Set(const int x, const int y, const int width, const int height);
  virtual void Clear();

  Rectangle &operator=(const Rectangle &rect);

  int x;
  int y;
  int width;
  int height;
};

} //  namespace oplot