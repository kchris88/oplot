#pragma once

// System includes
#include <limits>

namespace oplot {

template <class T>
class Range {
public:
  Range();
  Range(const T minX, const T maxX, const T minY, const T maxY);
  Range(const Range &other);
  ~Range();

  T RangeX() const;
  T RangeY() const;
  void Set(const T minX, const T maxX, const T minY, const T maxY);
  virtual void Clear();

  Range<T> &operator=(const Range<T> &range);

  T min_x;
  T max_x;
  T min_y;
  T max_y;
};

template <class T>
Range<T>::Range() {
  Clear();
}

template <class T>
Range<T>::Range(const T minX, const T maxX, const T minY, const T maxY)
  : min_x(minX),
    max_x(maxX),
    min_y(minY),
    max_y(maxY) {

}

template <class T>
Range<T>::Range(const Range<T> &other)
  : min_x(other.min_x),
    max_x(other.max_x),
    min_y(other.min_y),
    max_y(other.max_y) {

}

template <class T>
Range<T>::~Range() {
  min_x = static_cast<T>(0);
  max_x = static_cast<T>(0);
  min_y = static_cast<T>(0);
  max_y = static_cast<T>(0);
}

template <class T>
T Range<T>::RangeX() const {
  return max_x - min_x;
}

template <class T>
T Range<T>::RangeY() const {
  return max_y - min_y;
}

template <class T>
void Range<T>::Set(const T minX, const T maxX, const T minY, const T maxY) {
  this->min_x = minX;
  this->max_x = maxX;
  this->min_y = minY;
  this->max_y = maxY;
}

template <class T>
void Range<T>::Clear() {
  min_x = std::numeric_limits<T>::max();
  max_x = std::numeric_limits<T>::min();
  min_y = std::numeric_limits<T>::max();
  max_y = std::numeric_limits<T>::min();
}

template <class T>
Range<T> &Range<T>::operator=(const Range<T> &range) {
  min_x = range.min_x;
  max_x = range.max_x;
  min_y = range.min_y;
  max_y = range.max_y;
  return *this;
}

} // namespace oplot