#pragma once

namespace oplot {

template<class T>
class Step {
public:
  Step() = default;
  Step(const T x, const T y);
  Step(const Step<T> &other) = default;
  ~Step() = default;

  void Set(const T x, const T y);
  virtual void Clear();

  Step<T> &operator=(const Step<T> &step);

  T x;
  T y;
};

template<class T>
Step<T>::Step(const T x, const T y) 
  : x(x),
    y(y) {

}

template<class T>
void Step<T>::Set(const T x, const T y) {
  this->x = x;
  this->y = y;
}

template<class T>
void Step<T>::Clear() {
  x = static_cast<T>(0);
  y = static_cast<T>(0);
}

template<class T>
Step<T> &Step<T>::operator=(const Step<T> &step) {
  x = step.x;
  y = step.y;
  return *this;
}

} //  namespace oplot