#pragma once

#include <stdlib.h>
#include <vector>

namespace oplot {

class Color {
public:
  Color();
  Color(const double red, const double green, const double blue, const double alpha);
  Color(const Color &other) = default;
  ~Color() = default;

  static Color Generate(const int seed, const size_t data_size);

  void Set(const double red, const double green, const double blue, const double alpha);
  void Clear();

  Color &operator=(const Color &color);

  static const std::vector<Color> basic_color_vector;

  double alpha;
  double blue;
  double green;
  double red;
};

} // namespace oplot