#pragma once

// System includes
#include <string>
#include <vector>

namespace oplot {

class Legend {
public:
  enum : size_t {
    kPointMarker = 0,
    kRectangleMarker,
    kTotalNum
  };

  Legend() = default;
  Legend(const Legend &other) = default;
  ~Legend() = default;

  void Push(const std::vector<std::string> &legend_data);
  void Push(const std::string &legend_data);
  void Insert(size_t index, const std::vector<std::string> &legend_data);
  void Insert(size_t index, const std::string &legend_data);
  std::string Get(size_t index) const;
  std::vector<std::string> &legendData();
  void Clear();

protected:
  std::vector<std::string> _legend_data;
};

} //  namespace oplot