#pragma once

namespace oplot {

class Size {
public:
  Size();
  Size(const int width, const int height);
  Size(const Size &other) = default;
  ~Size() = default;

  void Set(const int width, const int height);
  virtual void Clear();
  
  Size &operator=(const Size &size);

  bool operator==(const Size &other) const;
  bool operator!=(const Size &other) const;

  int height;
  int width;
};

} // namespace oplot