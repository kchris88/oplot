#include <legend.h>
#include <iostream>

namespace oplot {

void Legend::Push(const std::vector<std::string> &legend_data) {
  _legend_data.insert(_legend_data.end(), legend_data.begin(), legend_data.end());
}

void Legend::Push(const std::string &legend_data) {
  _legend_data.push_back(legend_data);
}

void Legend::Insert(size_t index, const std::vector<std::string> &legend_data) {
  if (index <= _legend_data.size()) {
    _legend_data.insert(_legend_data.begin() + index, legend_data.begin(), legend_data.end());
  } else {
    throw std::runtime_error("Legend::Insert(size_t, const std::vector<std::string>&): Insert index is vector out of range.");
  }
}

void Legend::Insert(size_t index, const std::string &legend_data) {
  if (index <= _legend_data.size()) {
    _legend_data.insert(_legend_data.begin() + index, legend_data);
  } else {
    throw std::runtime_error("Legend::Insert(size_t, const std::string&): Insert index is vector out of range.");
  }
}

std::string Legend::Get(size_t index) const {
  if (index < _legend_data.size()) {
    return _legend_data[index];
  }
  throw std::runtime_error("Legend::Get(size_t) const: Index is out of range.");
}

std::vector<std::string> &Legend::legendData() {
  return _legend_data;
}

void Legend::Clear() {
  _legend_data.clear();
}

} //  namespace oplot