#include <size.h>

namespace oplot {

Size::Size()
  : height(0),
    width(0) {

}

Size::Size(const int width, const int height)
  : height(height),
    width(width) {

}

void Size::Set(const int width, const int height) {
  this->width = width;
  this->height = height;
}

void Size::Clear() {
  width = 0;
  height = 0;
}

Size &Size::operator=(const Size &size) {
  width = size.width;
  height = size.height;

  return *this;
}

bool Size::operator==(const Size &other) const {
  return (width == other.width && height == other.height);
}

bool Size::operator!=(const Size &other) const {
  return (width != other.width || height != other.height);
}

} // namespace oplot