#include <rectangle.h>

namespace oplot {

Rectangle::Rectangle()
  : x(0),
    y(0),
    width(0),
    height(0) {

}

Rectangle::Rectangle(const int x, const int y, const int width, const int height)
  : x(x),
    y(y),
    width(width),
    height(height) {

}

void Rectangle::Set(const int x, const int y, const int width, const int height) {
  this->x = x;
  this->y = y;
  this->width = width;
  this->height = height;
}

void Rectangle::Clear() {
  x = 0;
  y = 0;
  width = 0;
  height = 0;
}

} //  namespace oplot