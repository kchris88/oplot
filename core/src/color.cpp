#include <color.h>

// System includes
#include <math.h>

namespace oplot {

const std::vector<Color> Color::basic_color_vector = {
  Color(0.0, 0.0, 1.0, 0.0),
  Color(0.0, 1.0, 0.0, 0.0),
  Color(1.0, 0.0, 0.0, 0.0),
  Color(0.0, 1.0, 1.0, 0.0),
  Color(1.0, 0.0, 1.0, 0.0),
  Color(1.0, 1.0, 0.0, 0.0)};

Color::Color()
  : alpha(0.0),
    blue(0.0),
    green(0.0),
    red(0.0) {

}

Color::Color(const double red, const double green, const double blue, const double alpha)
  : alpha(alpha),
    blue(blue),
    green(green),
    red(red) {

}

Color Color::Generate(const int seed, const size_t data_size) {
  Color color;
	double color_nbr = static_cast<double>(data_size);
	
	if(color_nbr <= 6 || seed < 6) {
		return basic_color_vector[seed];
  } else {
    double step_value = ceil((color_nbr - 6) / 12) + 1;
		double step = 1.0 / step_value;
    double color_change = step * step_value;
		switch((seed - 6) % 12) {
			case 0: {
				color = basic_color_vector[0];
				color.blue -= color_change;
				return color;
      }
			case 1: {
				color = basic_color_vector[1];
				color.green -= color_change;
				return color;
      }
			case 2: {
				color = basic_color_vector[2];
				color.red -= color_change;
				return color;
      }
			case 3: {
				color = basic_color_vector[3];
				color.blue -= color_change;
				return color;
      }
			case 4: {
				color = basic_color_vector[4];
				color.blue -= color_change;
				return color;
      }
			case 5: {
				color = basic_color_vector[5];
				color.green -= color_change;
				return color;
      }
			case 6: {
				color = basic_color_vector[3];
				color.green -= color_change;
				return color;
      }
			case 7: {
				color = basic_color_vector[4];
				color.red -= color_change;
				return color;
      }
			case 8: {
				color = basic_color_vector[5];
				color.red -= color_change;
				return color;
      }
			case 9: {
				color = basic_color_vector[3];
				color.blue -= color_change;
				color.green -= color_change;
				return color;
      }
			case 10: {
				color = basic_color_vector[4];
				color.blue -= color_change;
				color.red -= color_change;
				return color;
      }
			case 11: {
				color = basic_color_vector[5];
				color.green -= color_change;
				color.red -= color_change;
				return color;
      }
			default:
				return Color();
		}
	}
}

void Color::Set(const double red, const double green, const double blue, const double alpha) {
  this->alpha = alpha;
  this->blue = blue;
  this->green = green;
  this->red = red;
}

void Color::Clear() {
  alpha = 0.0;
  blue = 0.0;
  green = 0.0;
  red = 0.0;
}

Color &Color::operator=(const Color &color) {
  alpha = color.alpha;
  blue = color.blue;
  green = color.green;
  red = color.red;
	return *this;
}

} // namespace oplot