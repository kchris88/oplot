#include <plot2d_core.h>

namespace oplot {

const double Plot2DCore::dash[] = {5.0, 5.0};
const int Plot2DCore::dash_length = sizeof(dash) / sizeof(dash[0]);

Plot2DCore::Plot2DCore(const Size &plot_size)
    : PlotCore(plot_size),
      settings(std::make_shared<Plot2DSettings>()) {

}

void Plot2DCore::Show(int delay_ms) {
  _window->SetTitle(settings->title);
  _window->SetDelay(delay_ms);
  if (_plot_surface_with_legend) {
    _window->Show(&_plot_surface_with_legend);
  } else {
    _window->Show(&_plot_surface);
  }
}

void Plot2DCore::QuickSave() {
  Save(settings->title);
}

void Plot2DCore::DrawLabels() {
  SetFont(_plot_ctx, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD, 20.0);
  cairo_text_extents_t extents;

  cairo_text_extents(_plot_ctx, settings->label_x.c_str(), &extents);
  double x_position = static_cast<double>((_plot_surface_size.width - extents.width) * 0.5);
  double y_position = static_cast<double>(_plot_surface_size.height - 60 + extents.height);
  DrawText(_plot_ctx, settings->label_x, x_position, y_position);

  cairo_text_extents(_plot_ctx, settings->label_y.c_str(), &extents);
  x_position = static_cast<double>(2 * extents.height);
  y_position = static_cast<double>((_plot_surface_size.height + extents.width) * 0.5);
  DrawText(_plot_ctx, settings->label_y, x_position, y_position, -M_PI_2);
}

void Plot2DCore::DrawAxisValues(cairo_t *ctx, const std::string &str, const double x_position, const double y_position, bool horizontal_axis) {
  cairo_text_extents_t extents;
  cairo_text_extents(ctx, str.c_str(), &extents);
  if (horizontal_axis) {
    DrawText(ctx, str, x_position - extents.width * 0.5, y_position + extents.height);
  } else {
    DrawText(ctx, str, x_position - extents.width, y_position + extents.height * 0.5);
  }
}

} //  namespace oplot