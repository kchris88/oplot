#include <plot_core.h>

// System includes
#include <iostream>
#include <sstream>
#include <sys/stat.h>

// Oplot includes
#include <context_guard.h>
#include <window_sdl.h>

namespace oplot {

PlotCore::PlotCore(const Size &plot_size)
  : _plot_surface(nullptr),
    _plot_surface_with_legend(nullptr),
    _plot_surface_size(Size(1000, 1000)) {
  _workspace_surface_rect.Set(_plot_surface_size.width * 0.1,
                              _plot_surface_size.height * 0.1,
                              _plot_surface_size.width * 0.8,
                              _plot_surface_size.height * 0.8);
  _plot_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, _plot_surface_size.width, _plot_surface_size.height);
  _window = std::make_shared<WindowSDL>("Oplot", plot_size);
}

void PlotCore::Save(const std::string &filename) {
  std::ostringstream str;
	struct stat buf;
	int ctr = 0;
	str << filename << ".png";
	while (stat(str.str().c_str(), &buf) == 0) {
		str.str("");
		str << filename << "_" << ++ctr << ".png";
	}
  if (_plot_surface_with_legend) {
    cairo_surface_write_to_png(_plot_surface_with_legend, str.str().c_str());
  } else {
	  cairo_surface_write_to_png(_plot_surface, str.str().c_str());
  }
}

Legend &PlotCore::legend() {
  return _legend;
}

Legend PlotCore::legend() const {
  return _legend;
}

void PlotCore::ClearImage() {
  cairo_t *_plot_ctx = cairo_create(_plot_surface);
  cairo_set_source_rgb(_plot_ctx, 1.0, 1.0, 1.0);
  cairo_paint(_plot_ctx);
  cairo_destroy(_plot_ctx);
  _plot_ctx = cairo_create(_plot_surface_with_legend);
  cairo_set_source_rgb(_plot_ctx, 1.0, 1.0, 1.0);
  cairo_paint(_plot_ctx);
  cairo_destroy(_plot_ctx);
}

void PlotCore::DrawLine(cairo_t *ctx, const double begin_x, const double begin_y, const double end_x, const double end_y, const double line_width) {
  cairo_move_to(ctx, begin_x, begin_y);
  cairo_line_to(ctx, end_x, end_y);
  cairo_set_line_width(ctx, line_width);
  cairo_stroke(ctx);
}

void PlotCore::DrawCircle(cairo_t *ctx, const double center_x, const double center_y, const double radius) {
  cairo_arc(ctx, center_x, center_y, radius, 0.0, 2*M_PI);
  cairo_fill(ctx);
  cairo_stroke(ctx);
}

void PlotCore::DrawRectangle(cairo_t *ctx, const double begin_x, const double begin_y, const double width, const double height) {
  cairo_rectangle(ctx, begin_x, begin_y, width, height);
  cairo_fill(ctx);
  cairo_stroke(ctx);
  cairo_set_source_rgb(ctx, 0.0, 0.0, 0.0);
  cairo_rectangle(ctx, begin_x, begin_y, width, height);
  cairo_stroke(ctx);
}

void PlotCore::DrawText(cairo_t *ctx, const std::string &text, const double begin_x, const double begin_y, const double angle) {
  cairo_set_source_rgb(ctx, 0.0, 0.0, 0.0);
  cairo_move_to(ctx, begin_x, begin_y);
  if (angle != 0.0) cairo_rotate(ctx, angle);
  cairo_show_text(ctx, text.c_str());
}

void PlotCore::SetFont(cairo_t *ctx, const _cairo_font_slant slant, const _cairo_font_weight weight, const double size) {
  cairo_select_font_face(ctx, "Sans", slant, weight);
  cairo_set_font_size(ctx, size);
}

int PlotCore::CalculateExponent(const double min, const double max) {
  int exponent = 0;
  double abs_max = (fabs(min) < fabs(max)) ? fabs(max) : fabs(min);
  if (abs_max > 1.0) {
    while(true) {
      if (abs_max < 10.0) break;
      abs_max /= 10.0;
      ++exponent;
    }
  } else if (abs_max < 0.1) {
    while(true) {
      if (abs_max > 1.0) break;
      abs_max *= 10.0;
      --exponent;
    }
  }
  return exponent;
}

void PlotCore::ReleaseImage() {
  if (_plot_surface) {
    cairo_surface_destroy(_plot_surface);
    _plot_surface = nullptr;
    cairo_surface_destroy(_plot_surface_with_legend);
    _plot_surface_with_legend = nullptr;
  }
}

void PlotCore::DrawLegend(const size_t plots_nbr, const size_t marker_type) {
  if (_legend.legendData().empty()) {
    if (_plot_surface_with_legend) {
      cairo_surface_destroy(_plot_surface_with_legend);
      _plot_surface_with_legend = nullptr;
    }
    return;
  }
  if (plots_nbr > _legend.legendData().size()) {
    throw std::runtime_error("Legend size and plots number must be the same.");
  }

  int max_length = 0;
  cairo_text_extents_t extents;
  for (const auto &element : _legend.legendData()) {
    cairo_text_extents(_plot_ctx, element.c_str(), &extents);
    if (max_length < extents.width) {
      max_length = extents.width;
    }
  }
  PreparePlotSurfaceWithLegend(max_length);

  cairo_t *plot_ctx;
  ContextGuard plot_with_legend_guard(_plot_surface_with_legend, &plot_ctx);
  cairo_set_source_rgb(plot_ctx, 1.0, 1.0, 1.0);
  cairo_paint(plot_ctx);
  cairo_set_source_surface(plot_ctx, _plot_surface, 0.0, 0.0);
  cairo_paint(plot_ctx);

  SetFont(plot_ctx, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL, 20.0);
  int paste_y = (_plot_surface_size.height - _workspace_surface_rect.height + _workspace_surface_rect.y) >> 1;

  for (size_t i = 0; i < plots_nbr; ++i) {
    cairo_set_source_rgb(plot_ctx, _color[i].red, _color[i].green, _color[i].blue);
    int x_begin_position = _plot_surface_size.width;
    int y_begin_position = paste_y + 10 + (extents.height * 0.5 + 20) * i;
    int x_end_position = x_begin_position + 50;
    int y_end_position = y_begin_position;
    switch (marker_type) {
      case Legend::kPointMarker: {
        DrawLine(plot_ctx, x_begin_position, y_begin_position, x_end_position, y_end_position, 2.0);
        DrawCircle(plot_ctx, x_begin_position + 25, y_begin_position, 5.0);
        cairo_fill(plot_ctx);
        cairo_stroke(plot_ctx);
        break;
      }
      case Legend::kRectangleMarker: {
        double x_position = _plot_surface_size.width + 30;
        double y_position = static_cast<double>(paste_y + 10 + (extents.height * 0.5 + 20) * i - extents.height * 0.6);
        DrawRectangle(plot_ctx, x_position, y_position, 20.0, 20.0);
        break;
      }
      default: {
        throw std::invalid_argument("Wrong legend marker type.");
      }
    }
    DrawText(plot_ctx, _legend.Get(i), x_begin_position + 60, y_begin_position + extents.height * 0.4);
  }
}

void PlotCore::PreparePlotSurfaceWithLegend(const int max_legend_width) {
  if (!_plot_surface_with_legend) {
    _plot_surface_with_legend_size = _plot_surface_size;
    _plot_surface_with_legend_size.width += 130 + max_legend_width;
    _plot_surface_with_legend = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 
                                                           _plot_surface_with_legend_size.width, 
                                                           _plot_surface_with_legend_size.height);
  }
}

} //  namespace oplot