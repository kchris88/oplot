#include <plot2d_impl.h>

// System includes
#include <algorithm>
#include <iostream>
#include <sstream>

// Oplot includes
#include <context_guard.h>

namespace oplot {

Plot2DImpl::Plot2DImpl(const Size &plot_size)
  : Plot2DCore(plot_size) {
      
}

void Plot2DImpl::Draw(const std::vector<double> &y_data, const std::vector<double> &x_data) {
  if (!x_data.empty()) {
    _x_data.push_back(x_data);
  }
  _y_data.push_back(y_data);
  
  DrawFromData();
}

void Plot2DImpl::Draw(const std::vector<std::vector<double>> &y_data, const std::vector<std::vector<double>> &x_data) {
  if (!x_data.empty()) {
    _x_data.insert(_x_data.end(), x_data.begin(), x_data.end());
  }
  _y_data.insert(_y_data.end(), y_data.begin(), y_data.end());

  DrawFromData();
}

void Plot2DImpl::Release() {
  Clear();
  settings->Clear();

  ReleaseImage();
}

void Plot2DImpl::Clear() {
  _color.clear();
  _legend.Clear();

  ClearData();
  ClearImage();
}

void Plot2DImpl::DrawAxis() {
  int paste_x = _workspace_surface_rect.x;
  int paste_y = _workspace_surface_rect.y;
  int end_x = paste_x + _workspace_surface_rect.width;
  int end_y = paste_y + _workspace_surface_rect.height;

  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);
  DrawLine(_plot_ctx, paste_x, paste_y, paste_x, end_y, 3.0);
  DrawLine(_plot_ctx, paste_x, end_y, end_x, end_y, 3.0);

  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);

  Step<double> key_step = SetStepFromRange();
  
  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

  double begin_y_position = static_cast<double>(end_y - 5.0);
  double end_y_position = static_cast<double>(end_y + 5.0);
  if (settings->x_key_values && !_x_data.empty() && !_x_data.front().empty()) {  
    auto x_range_shift = settings->GetRange().min_x + settings->shift.x;
    for (const auto &x_vector : _x_data) {
      for (const auto &x : x_vector) {
        double x_position = paste_x + (x - x_range_shift) * key_step.x;
        DrawLine(_plot_ctx, x_position, begin_y_position, x_position, end_y_position, 2.0);
      }
    }
  } else {
    for (int i = 1; i <= settings->axis_step.x; ++i) {
      double x_position = static_cast<double>(paste_x + i * step.x);
      DrawLine(_plot_ctx, x_position, begin_y_position, x_position, end_y_position, 3.0);
    }
  }
  double begin_x_position = static_cast<double>(paste_x - 5.0);
  double end_x_position = static_cast<double>(paste_x + 5.0);
  if (settings->y_key_values) {
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    auto y_paste_shift = paste_y + _workspace_surface_rect.height;
    for (const auto &y_vector : _y_data) {
      for (const auto &y : y_vector) {
        double y_position = static_cast<double>(y_paste_shift - (y - y_range_shift) * key_step.y);
        DrawLine(_plot_ctx, begin_x_position, y_position, end_x_position, y_position, 2.0);
      }
    }
  } else {
    for (int i = 0; i < settings->axis_step.y; ++i) {
      double y_position = static_cast<double>(paste_y + i * step.y);
      DrawLine(_plot_ctx, begin_x_position, y_position, end_x_position, y_position, 3.0);
    }
  }
  DrawAxisDescription();
  DrawLabels();
}

void Plot2DImpl::DrawAxisDescription() {
  int paste_x = _workspace_surface_rect.x;
  int paste_y = _workspace_surface_rect.y;
  int end_x = paste_x + _workspace_surface_rect.width;
  int end_y = paste_y + _workspace_surface_rect.height;

  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);

  Step<double> key_step = SetStepFromRange();

  std::ostringstream str;
  str.setf(std::ios_base::fixed);

  cairo_select_font_face(_plot_ctx, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size(_plot_ctx, 20.0);
  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

  int exponent = CalculateExponent(settings->GetRange().min_x, settings->GetRange().max_x);
  double data_multiplier = std::pow(10.0, -exponent);
  if (settings->x_key_values && !_x_data.empty() && !_x_data.front().empty()) {
    str.precision(2);
    auto x_range_shift = settings->GetRange().min_x + settings->shift.x;
    for (const auto &x_vector : _x_data) {
      for (const auto &x : x_vector) {
        str.str("");
        double data = x * data_multiplier;
        str << data;
        double x_position = static_cast<double>(paste_x + (x - x_range_shift) * key_step.x);
        double y_position = static_cast<double>(end_y + 8);
        DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
      }
    }
  } else {
    str.precision(1);
    for (int i = 0; i <= settings->axis_step.x; ++i) {
      str.str("");
      double data = (settings->GetRange().min_x + i * settings->GetRange().RangeX() / static_cast<double>(settings->axis_step.x)) * data_multiplier;
      str << data;
      double x_position = static_cast<double>(paste_x + i * step.x);
      double y_position = static_cast<double>(end_y + 8);
      DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
    }
  }
  if (exponent != 0) {
    double x_position = static_cast<double>(end_x + 35);
    double y_position = static_cast<double>(end_y + 14);
    DrawMultiplier(exponent, x_position, y_position);
  }
  exponent = CalculateExponent(settings->GetRange().min_y, settings->GetRange().max_y);
  data_multiplier = std::pow(10.0, -exponent);
  if(settings->y_key_values) {
    str.precision(2);
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    auto y_paste_shift = paste_y + _workspace_surface_rect.height;
    for (const auto &y_vector : _y_data) {
      for (const auto &y : y_vector) {
        str.str("");
        double data = y * data_multiplier;
        str << data;
        double x_position = static_cast<double>(paste_x - 8);
        double y_position = static_cast<double>(y_paste_shift - (y - y_range_shift) * key_step.y);
        DrawAxisValues(_plot_ctx, str.str(), x_position, y_position, false);
      }
    }
  } else {
    str.precision(1);
    for (int i = 0; i <= settings->axis_step.y; ++i) {
      str.str("");
      double data = (settings->GetRange().min_y + i * settings->GetRange().RangeY() / static_cast<double>(settings->axis_step.y)) * data_multiplier;
      str << data;
      double x_position = static_cast<double>(paste_x - 8);
      double y_position = static_cast<double>(end_y - i * step.y);
      DrawAxisValues(_plot_ctx, str.str(), x_position, y_position, false);
    }
  }
  if (exponent != 0) {
    double x_position = static_cast<double>(paste_x - 10);
    double y_position = static_cast<double>(paste_y - 30);
    DrawMultiplier(exponent, x_position, y_position);
  }
}

void Plot2DImpl::DrawGrid() {
  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);
  cairo_set_source_rgb(_plot_ctx, 0.58, 0.58, 0.58);
  cairo_set_dash(_plot_ctx, dash, dash_length, 1.0);
  for (int i = 1; i <= settings->axis_step.x; ++i) {
    double x_position = static_cast<double>(i * step.x + _workspace_surface_rect.x);
    DrawLine(_plot_ctx, x_position, _workspace_surface_rect.y, x_position, _workspace_surface_rect.y + _workspace_surface_rect.height, 1.0);
  }
  for (int i = 0; i < settings->axis_step.y; ++i) {
    double y_position = static_cast<double>(i * step.y + _workspace_surface_rect.y);
    DrawLine(_plot_ctx, _workspace_surface_rect.x, y_position, _workspace_surface_rect.x + _workspace_surface_rect.width, y_position, 1.0);
  }
  cairo_set_dash(_plot_ctx, dash, 0.0, 0.0);
}

void Plot2DImpl::DrawPlot(const Step<double> &data_step) {
  cairo_surface_t *tmp_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, _workspace_surface_rect.width, _workspace_surface_rect.height);
  cairo_t *tmp_ctx = cairo_create(tmp_surface);
  cairo_set_source_surface(tmp_ctx, _plot_surface, -_workspace_surface_rect.x, -_workspace_surface_rect.y);
  cairo_rectangle(tmp_ctx, 0.0, 0.0, _workspace_surface_rect.width, _workspace_surface_rect.height);
  cairo_fill(tmp_ctx);

  for (size_t i = 0; i < _y_data.size(); ++i) {
    if (_y_data.empty()) {
      throw std::runtime_error("ERROR Plot::Draw(): At least one of the input data vector is empty.");
    }
    cairo_set_source_rgb(tmp_ctx, _color[i].red, _color[i].green, _color[i].blue);
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    if (_x_data.size() <= i || _x_data[i].empty()) {
      for (size_t j = 1; j < _y_data[i].size(); ++j) {
        double begin_y_position = _workspace_surface_rect.height - (_y_data[i][j - 1] - y_range_shift) * data_step.y;
        double begin_x_position = (j - 1 + settings->shift.x) * data_step.x;
        double end_x_position = (j + settings->shift.x) * data_step.x;
        double end_y_position = _workspace_surface_rect.height - (_y_data[i][j] - y_range_shift) * data_step.y;
        DrawLine(tmp_ctx, begin_x_position, begin_y_position, end_x_position, end_y_position, 2.0);
      }
    } else {
      if (_y_data[i].size() != _x_data[i].size()) {
        throw std::runtime_error("ERROR Plot::Draw(): X and Y data vectors must to have the same size.");
      }
      auto x_range_shift = settings->GetRange().min_x + settings->shift.x;
      for (size_t j = 1; j < _y_data[i].size(); ++j) {
        double begin_x_position = (_x_data[i][j-1] - x_range_shift) * data_step.x;
        double begin_y_position = _workspace_surface_rect.height - (_y_data[i][j - 1] - y_range_shift) * data_step.y;
        double end_x_position = (_x_data[i][j] - x_range_shift) * data_step.x;
        double end_y_position = _workspace_surface_rect.height - (_y_data[i][j] - y_range_shift) * data_step.y;
        DrawLine(tmp_ctx, begin_x_position, begin_y_position, end_x_position, end_y_position, 2.0);
      }
    }
  }
  cairo_set_source_surface(_plot_ctx, tmp_surface, _workspace_surface_rect.x, _workspace_surface_rect.y);
  cairo_paint(_plot_ctx);
  cairo_destroy(tmp_ctx);
  cairo_surface_destroy(tmp_surface);
}

void Plot2DImpl::DrawMeasurePoints(const Step<double> &data_step) {
  auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
  for (size_t i = 0; i < _y_data.size(); ++i) {
    if (_x_data.empty() || _x_data[i].empty()) {
      for (size_t j = 0; j < _y_data[i].size(); ++j) {
        int x_position = static_cast<int>(_workspace_surface_rect.x + (j + settings->shift.x) * data_step.x);
        int y_position = static_cast<int>(_workspace_surface_rect.y + _workspace_surface_rect.height - (_y_data[i][j] - y_range_shift) * data_step.y);
        if (x_position >= 0 && x_position <= (_workspace_surface_rect.x + _workspace_surface_rect.width) && 
            y_position >= 0 && y_position <= (_workspace_surface_rect.y + _workspace_surface_rect.height)) {
          cairo_set_source_rgb(_plot_ctx, _color[i].red, _color[i].green, _color[i].blue);
          DrawCircle(_plot_ctx, static_cast<double>(x_position), static_cast<double>(y_position), 5.0);
          if (settings->x_key_values || settings->y_key_values) {
            MarkKeyValues(_plot_ctx, static_cast<double>(x_position), static_cast<double>(y_position));
          }
        }
      }
    } else {
      auto x_range_shift = settings->GetRange().min_x + settings->shift.x;
      for (size_t j = 0; j < _y_data[i].size(); ++j) {
        int x_position = static_cast<int>((_x_data[i][j] - x_range_shift) * data_step.x);
        int y_position = static_cast<int>(_workspace_surface_rect.height - (_y_data[i][j] - y_range_shift) * data_step.y);
        if (x_position >= 0 && x_position <= _workspace_surface_rect.width && 
            y_position >= 0 && y_position <= _workspace_surface_rect.height) {
          cairo_set_source_rgb(_plot_ctx, _color[i].red, _color[i].green, _color[i].blue);
          DrawCircle(_plot_ctx, static_cast<double>(_workspace_surface_rect.x + x_position), static_cast<double>(_workspace_surface_rect.y + y_position), 5.0);
          if (settings->x_key_values || settings->y_key_values) {
            MarkKeyValues(_plot_ctx, static_cast<double>(_workspace_surface_rect.x + x_position), static_cast<double>(_workspace_surface_rect.y + y_position));
          }
        }
      }
    }
  }
}

void Plot2DImpl::MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position) {
  cairo_set_source_rgb(ctx, 0.0, 0.0, 0.0);
  cairo_set_dash(ctx, dash, dash_length, 1.0);
  if (settings->x_key_values) {
    DrawLine(ctx, x_position, y_position, x_position, static_cast<double>(_workspace_surface_rect.y + _workspace_surface_rect.height), 1.0);
  }
  if (settings->y_key_values) {
    DrawLine(ctx, x_position, y_position, _workspace_surface_rect.x, y_position, 1.0);
  }
  cairo_set_dash(ctx, dash, 0.0, 0.0);
}

void Plot2DImpl::DrawMultiplier(int exponent, double x_position, double y_position) {
  std::ostringstream str;
  cairo_text_extents_t extents;
  cairo_text_extents_t extents_exp;
  str.precision(0);
  str.str("");
  str << "x10";
  cairo_text_extents(_plot_ctx, str.str().c_str(), &extents);
  DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  str.str("");
  str << exponent;
  cairo_set_font_size(_plot_ctx, 14.0);
  cairo_text_extents(_plot_ctx, str.str().c_str(), &extents_exp);
  x_position = x_position + (extents.width + extents_exp.width) * 0.5;
  y_position = y_position - 8;
  DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  cairo_set_font_size(_plot_ctx, 20.0);
}

void Plot2DImpl::PrepareColors() {
  for (size_t i = 0; i < _y_data.size(); ++i) {
    if (_y_data.empty()) {
      throw std::runtime_error("ERROR Plot::Draw(): At least one of the input data vector is empty.");
    }
    _color.emplace_back(Color::Generate(i, _y_data.size()));
  }
}

void Plot2DImpl::SetMinMaxRange() {
  if (!settings->IsRangeSet()) {
    settings->GetRange().Clear();
    for (size_t i = 0; i < _y_data.size(); ++i) {
      SetRangeFromXData(i);
      SetRangeFromYData(i);
    }
  }
}

void Plot2DImpl::SetRangeFromXData(size_t data_index) {
  double min, max;
  auto y_vector = _y_data[data_index];
  if (_x_data.empty()) {
    min = 0.0;
    max = y_vector.size() - 1;
  } else {
    auto x_vector = _x_data[data_index];
    if (x_vector.empty()) {
      min = 0.0;
      max = y_vector.size() - 1;
    } else {
      min = *std::min_element(x_vector.begin(), x_vector.end());
      max = *std::max_element(x_vector.begin(), x_vector.end());
    }
  }

  auto actual_range = settings->GetRange();
  if (actual_range.min_x > min) actual_range.min_x = min;
  if (actual_range.max_x < max) actual_range.max_x = max;
  settings->SetRange(actual_range, !settings->IsRangeSet());
}

void Plot2DImpl::SetRangeFromYData(size_t data_index) {
  auto y_vector = _y_data[data_index];
  double min = *std::min_element(y_vector.begin(), y_vector.end());
  double max = *std::max_element(y_vector.begin(), y_vector.end());
  auto actual_range = settings->GetRange();
  if (actual_range.min_y > min) actual_range.min_y = min;
  if (actual_range.max_y < max) actual_range.max_y = max;
  settings->SetRange(actual_range, !settings->IsRangeSet());
}

Step<double> Plot2DImpl::SetStepFromRange() {
  Step<double> step;
  step.x = _workspace_surface_rect.width / settings->GetRange().RangeX();
  step.y = _workspace_surface_rect.height / settings->GetRange().RangeY();
  return step;
}

void Plot2DImpl::ClearData() {
  _x_data.clear();
  _y_data.clear();
}

void Plot2DImpl::DrawFromData() {
  _color.clear();
  ClearImage();
  srand(time(NULL));

  SetMinMaxRange();

  ContextGuard plot_guard(_plot_surface, &_plot_ctx);

  if (settings->grid_on) DrawGrid();
  auto data_step = SetStepFromRange();
  PrepareColors();
  if (settings->plot_lines_on) DrawPlot(data_step);
  if (settings->measure_points_on) DrawMeasurePoints(data_step);
  DrawAxis();
  DrawLegend(_y_data.size());
}

} //  namespace oplot