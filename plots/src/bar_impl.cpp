#include <bar_impl.h>

// System includes
#include <algorithm>
#include <iostream>
#include <sstream>

// Oplot includes
#include <context_guard.h>

namespace oplot {

BarImpl::BarImpl(const Size &plot_size)
  : BarCore(plot_size) {

}

void BarImpl::Draw(const std::vector<double> &data, const std::string &label) {
  _data.push_back(data);
  _labels.push_back(label);

  DrawFromData();
}

void BarImpl::Draw(const std::vector<std::vector<double>> &data, const std::vector<std::string> &labels) {
  if (data.size() != labels.size()) {
    throw std::invalid_argument("Data and labels vectors must have the same size.");
  }
  _data.insert(_data.end(), data.begin(), data.end());
  _labels.insert(_labels.end(), labels.begin(), labels.end());

  DrawFromData();
}

void BarImpl::Release() {
  Clear();
  settings->Clear();

  ReleaseImage();
}

void BarImpl::Clear() {
  _color.clear();

  ClearData();
  ClearImage();
}

void BarImpl::DrawAxis() {
  int paste_x = _workspace_surface_rect.x;
  int paste_y = _workspace_surface_rect.y;
  int end_x = paste_x + _workspace_surface_rect.width;
  int end_y = paste_y + _workspace_surface_rect.height;

  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);
  DrawLine(_plot_ctx, paste_x, paste_y, paste_x, end_y, 3.0);
  DrawLine(_plot_ctx, paste_x, end_y, end_x, end_y, 3.0);

  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);

  Step<double> key_step = SetStepFromRange();
  
  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

  double begin_x_position = static_cast<double>(paste_x - 5.0);
  double end_x_position = static_cast<double>(paste_x + 5.0);
  if (settings->key_values) {
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    auto y_paste_shift = paste_y + _workspace_surface_rect.height;
    for (const auto &data_vec : _data) {
      for (const auto &element : data_vec) {
        double y_position = y_paste_shift - (element - y_range_shift) * key_step.y;
        DrawLine(_plot_ctx, begin_x_position, y_position, end_x_position, y_position, 2.0);
      }
    }
  } else {
    for (int i = 0; i < settings->axis_step.y; ++i) {
      double y_position = static_cast<double>(paste_y + i * step.y);
      DrawLine(_plot_ctx, begin_x_position, y_position, end_x_position, y_position, 3.0);
    }
  }
  DrawAxisDescription();
  DrawLabels();
}

void BarImpl::DrawAxisDescription() {
  int paste_x = _workspace_surface_rect.x;
  int paste_y = _workspace_surface_rect.y;
  int end_x = paste_x + _workspace_surface_rect.width;
  int end_y = paste_y + _workspace_surface_rect.height;

  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);

  Step<double> key_step = SetStepFromRange();

  std::ostringstream str;
  str.setf(std::ios_base::fixed);

  cairo_select_font_face(_plot_ctx, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size(_plot_ctx, 20.0);
  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

  double bin_size = _workspace_surface_rect.width / static_cast<double>(_data.size());
  for (size_t i = 0; i < _labels.size(); ++i) {
    double x_position = paste_x + ((static_cast<double>(i) + 0.5) * bin_size);
    double y_position = static_cast<double>(end_y + 8);
    DrawAxisValues(_plot_ctx, _labels[i], x_position, y_position);
  }
  double exponent = CalculateExponent(settings->GetRange().min_y, settings->GetRange().max_y);
  double data_multiplier = std::pow(10.0, -exponent);
  if (settings->key_values) {
    str.precision(2);
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    auto y_paste_shift = paste_y + _workspace_surface_rect.height;
    for (const auto &data_vec : _data) {
      for (const auto &element : data_vec) {
        str.str("");
        double data = element * data_multiplier;
        str << data;
        double x_position = static_cast<double>(paste_x - 8);
        double y_position = static_cast<double>(y_paste_shift - (element - y_range_shift) * key_step.y);
        DrawAxisValues(_plot_ctx, str.str(), x_position, y_position, false);
      }
    }
  } else {
    str.precision(1);
    for (int i = 0; i <= settings->axis_step.y; ++i) {
      str.str("");
      double data = (settings->GetRange().min_y + i * settings->GetRange().RangeY() / static_cast<double>(settings->axis_step.y)) * data_multiplier;
      str << data;
      double x_position = static_cast<double>(paste_x - 8);
      double y_position = static_cast<double>(end_y - i * step.y);
      DrawAxisValues(_plot_ctx, str.str(), x_position, y_position, false);
    }
  }
  if (exponent != 0) {
    double x_position = static_cast<double>(paste_x - 10);
    double y_position = static_cast<double>(paste_y - 30);
    DrawMultiplier(exponent, x_position, y_position);
  }
}

void BarImpl::DrawGrid() {
  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);
  cairo_set_source_rgb(_plot_ctx, 0.58, 0.58, 0.58);
  cairo_set_dash(_plot_ctx, dash, dash_length, 1.0);
  for (int i = 0; i < settings->axis_step.y; ++i) {
    double y_position = static_cast<double>(i) * step.y + static_cast<double>(_workspace_surface_rect.y);
    DrawLine(_plot_ctx, _workspace_surface_rect.x, y_position, _workspace_surface_rect.x + _workspace_surface_rect.width, y_position, 1.0);
  }
  cairo_set_dash(_plot_ctx, dash, 0.0, 0.0);
}

void BarImpl::DrawBar(const double step_y) {
  cairo_surface_t *tmp_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, _workspace_surface_rect.width, _workspace_surface_rect.height);
  cairo_t *tmp_ctx = cairo_create(tmp_surface);
  cairo_set_source_surface(tmp_ctx, _plot_surface, -_workspace_surface_rect.x, -_workspace_surface_rect.y);
  cairo_rectangle(tmp_ctx, 0.0, 0.0, _workspace_surface_rect.width, _workspace_surface_rect.height);
  cairo_fill(tmp_ctx);

  double bin_size = _workspace_surface_rect.width / static_cast<double>(_data.size());
  double sub_bin_size = bin_size / (static_cast<double>(_data.front().size()) + 0.5);
  double bin_space = (bin_size - sub_bin_size * _data.front().size()) * 0.5;
  double baseline = (settings->GetRange().min_y < 0.0) ? fabs(settings->GetRange().min_y) : 0.0;

  double y_shift = _workspace_surface_rect.height - baseline * step_y;

  for (size_t j = 0; j < _data.size(); ++j) {
    for (size_t i = 0; i < _data[j].size(); ++i) {
      double x_position = j * bin_size + i * sub_bin_size + bin_space;
      double y_position = (-_data[j][i]) * step_y + y_shift;
      if (!settings->single_color && _data[j].size() == 1) {
        cairo_set_source_rgb(tmp_ctx, _color[j].red, _color[j].green, _color[j].blue);
      } else {
        cairo_set_source_rgb(tmp_ctx, _color[i].red, _color[i].green, _color[i].blue);
      }
      DrawRectangle(tmp_ctx, x_position, y_shift, sub_bin_size, (-_data[j][i])*step_y);
      MarkKeyValues(tmp_ctx, x_position, y_position);
    }
  }
  cairo_set_source_surface(_plot_ctx, tmp_surface, _workspace_surface_rect.x, _workspace_surface_rect.y);
  cairo_paint(_plot_ctx);
  cairo_destroy(tmp_ctx);
  cairo_surface_destroy(tmp_surface);
}

void BarImpl::MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position)  {
  if (settings->key_values) {
    cairo_set_source_rgb(ctx, 0.0, 0.0, 0.0);
    cairo_set_dash(ctx, dash, dash_length, 1.0);
    DrawLine(ctx, x_position, y_position, _workspace_surface_rect.x, y_position, 1.0);
    cairo_set_dash(_plot_ctx, dash, 0.0, 0.0);
  }
}

void BarImpl::DrawMultiplier(int exponent, double x_position, double y_position) {
  std::ostringstream str;
  cairo_text_extents_t extents;
  cairo_text_extents_t extents_exp;
  str.precision(0);
  str.str("");
  str << "x10";
  cairo_text_extents(_plot_ctx, str.str().c_str(), &extents);
  DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  str.str("");
  str << exponent;
  cairo_set_font_size(_plot_ctx, 14.0);
  cairo_text_extents(_plot_ctx, str.str().c_str(), &extents_exp);
  x_position = x_position + (extents.width + extents_exp.width) * 0.5;
  y_position = y_position - 8;
  DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  cairo_set_font_size(_plot_ctx, 20.0);
}

void BarImpl::PrepareColors() {
  for (size_t i = 0; i < _data.size(); ++i) {
    if (!settings->single_color && _data[i].size() == 1) {
      _color.push_back(Color::Generate(i, 1));
    } else {
      for (size_t j = 0; j < _data[i].size(); ++j) {
        _color.push_back(std::move(Color::Generate(j, _data[i].size())));
      }
    }
  }
}

void BarImpl::SetMinMaxRange() {
  if (!settings->IsRangeSet()) {
    Range<double> range;
    for (const auto &data_vec : _data) {
      double min_data = *std::min_element(data_vec.begin(), data_vec.end());
      double max_data = *std::max_element(data_vec.begin(), data_vec.end());
      if (range.min_y > min_data) range.min_y = min_data;
      if (range.max_y < max_data) range.max_y = max_data;
    }
    range.min_x = 0.0;
    range.max_x = 0.0;
    settings->SetRange(range, !settings->IsRangeSet());
  }
}

Step<double> BarImpl::SetStepFromRange() {
  Step<double> step;
  step.x = _workspace_surface_rect.width / settings->GetRange().RangeX();
  step.y = _workspace_surface_rect.height / settings->GetRange().RangeY();
  return step;
}

void BarImpl::ClearData() {
  _data.clear();
  _labels.clear();
}

void BarImpl::DrawFromData() {
  _color.clear();
  ClearImage();
  SetMinMaxRange();
  PrepareColors();

  ContextGuard plot_guard(_plot_surface, &_plot_ctx);

  if (settings->grid_on) DrawGrid();

  double step_y = _workspace_surface_rect.height / settings->GetRange().RangeY();
  
  DrawBar(step_y);
  DrawAxis();
  DrawLegend(_data.front().size(), Legend::kRectangleMarker);
}

} //  namespace oplot