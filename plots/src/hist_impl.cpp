#include <hist_impl.h>

// System includes
#include <algorithm>
#include <iostream>
#include <sstream>

// Oplot includes
#include <context_guard.h>

namespace oplot {

HistImpl::HistImpl(const Size &plot_size)
  : HistCore(plot_size) {

}

void HistImpl::Draw(const std::vector<double> &data) {
  _color.clear();
  ClearImage();
  srand(time(NULL));

  _data_min = *std::min_element(data.begin(), data.end());
  _data_max = *std::max_element(data.begin(), data.end());

  SortData(data);
  SetMinMaxRange();

  ContextGuard plot_guard(_plot_surface, &_plot_ctx);

  PrepareColors();
  if (settings->grid_on) DrawGrid();

  auto step = SetStepFromRange();

  DrawHist(step);
  DrawAxis();
}

void HistImpl::Release() {
  Clear();
  settings->Clear();

  ReleaseImage();
}

void HistImpl::Clear() {
  _color.clear();

  ClearData();
  ClearImage();
}

void HistImpl::DrawAxis() {
  int paste_x = _workspace_surface_rect.x;
  int paste_y = _workspace_surface_rect.y;
  int end_x = paste_x + _workspace_surface_rect.width;
  int end_y = paste_y + _workspace_surface_rect.height;

  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);
  DrawLine(_plot_ctx, paste_x, paste_y, paste_x, end_y, 3.0);
  DrawLine(_plot_ctx, paste_x, end_y, end_x, end_y, 3.0);

  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);

  Step<double> key_step = SetStepFromRange();
  
  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

  double begin_y_position = static_cast<double>(end_y - 5.0);
  double end_y_position = static_cast<double>(end_y + 5.0);
  for (int i = 1; i <= settings->axis_step.x; ++i) {
    double x_position = static_cast<double>(paste_x + i * step.x);
    DrawLine(_plot_ctx, x_position, begin_y_position, x_position, end_y_position, 3.0);
  }
  double begin_x_position = static_cast<double>(paste_x - 5.0);
  double end_x_position = static_cast<double>(paste_x + 5.0);
  if (settings->key_values) {
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    auto y_paste_shift = paste_y + _workspace_surface_rect.height;
    for (const auto &y : _data) {
      double y_position = static_cast<double>(y_paste_shift - (y - y_range_shift) * key_step.y);
      DrawLine(_plot_ctx, begin_x_position, y_position, end_x_position, y_position, 2.0);
    }
  } else {
    for (int i = 0; i < settings->axis_step.y; ++i) {
      double y_position = static_cast<double>(paste_y + i * step.y);
      DrawLine(_plot_ctx, begin_x_position, y_position, end_x_position, y_position, 3.0);
    }
  }
  DrawAxisDescription();
  DrawLabels();
}

void HistImpl::DrawAxisDescription() {
  int paste_x = _workspace_surface_rect.x;
  int paste_y = _workspace_surface_rect.x;
  int end_x = paste_x + _workspace_surface_rect.width;
  int end_y = paste_y + _workspace_surface_rect.height;

  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);

  Step<double> key_step = SetStepFromRange();

  std::ostringstream str;
  str.setf(std::ios_base::fixed);

  cairo_select_font_face(_plot_ctx, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size(_plot_ctx, 20.0);
  cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

  str.precision(1);
  double exponent = CalculateExponent(settings->GetRange().min_x, settings->GetRange().max_x);
  double data_multiplier = std::pow(10.0, -exponent);
  for (int i = 0; i <= settings->axis_step.x; ++i) {
    str.str("");
    double data = (settings->GetRange().min_x + i * settings->GetRange().RangeX() / static_cast<double>(settings->axis_step.x)) * data_multiplier;
    str << data;
    double x_position = static_cast<double>(paste_x + i * step.x);
    double y_position = static_cast<double>(end_y + 8);
    DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  }
  if (exponent != 0) {
    double x_position = static_cast<double>(end_x + 35);
    double y_position = static_cast<double>(end_y + 14);
    DrawMultiplier(exponent, x_position, y_position);
  }
  exponent = CalculateExponent(settings->GetRange().min_y, settings->GetRange().max_y);
  data_multiplier = std::pow(10.0, -exponent);
  if(settings->key_values) {
    str.precision(2);
    auto y_range_shift = settings->GetRange().min_y + settings->shift.y;
    auto y_paste_shift = paste_y + _workspace_surface_rect.height;
    for (const auto &y : _data) {
      str.str("");
      double data = y * data_multiplier;
      str << data;
      double x_position = static_cast<double>(paste_x - 8);
      double y_position = static_cast<double>(y_paste_shift - (y - y_range_shift) * key_step.y);
      DrawAxisValues(_plot_ctx, str.str(), x_position, y_position, false);
    }
  } else {
    str.precision(1);
    for (int i = 0; i <= settings->axis_step.y; ++i) {
      str.str("");
      double data = (settings->GetRange().min_y + i * settings->GetRange().RangeY() / static_cast<double>(settings->axis_step.y)) * data_multiplier;
      str << data;
      double x_position = static_cast<double>(paste_x - 8);
      double y_position = static_cast<double>(end_y - i * step.y);
      DrawAxisValues(_plot_ctx, str.str(), x_position, y_position, false);
    }
  }
  if (exponent != 0) {
    double x_position = static_cast<double>(paste_x - 10);
    double y_position = static_cast<double>(paste_y - 30);
    DrawMultiplier(exponent, x_position, y_position);
  }
}

void HistImpl::DrawGrid() {
  Step<double> step;
  step.x = _workspace_surface_rect.width / static_cast<double>(settings->axis_step.x);
  step.y = _workspace_surface_rect.height / static_cast<double>(settings->axis_step.y);
  cairo_set_source_rgb(_plot_ctx, 0.58, 0.58, 0.58);
  cairo_set_dash(_plot_ctx, dash, dash_length, 1.0);
  for (int i = 1; i <= settings->axis_step.x; ++i) {
    double x_position = static_cast<double>(i * step.x + _workspace_surface_rect.x);
    DrawLine(_plot_ctx, x_position, _workspace_surface_rect.y, x_position, _workspace_surface_rect.height, 1.0);
  }
  for (int i = 0; i < settings->axis_step.y; ++i) {
    double y_position = static_cast<double>(i * step.y + _workspace_surface_rect.y);
    DrawLine(_plot_ctx, _workspace_surface_rect.x, y_position, _workspace_surface_rect.width, y_position, 1.0);
  }
  cairo_set_dash(_plot_ctx, dash, 0.0, 0.0);
}

void HistImpl::DrawHist(const Step<double> &data_step) {
  cairo_surface_t *tmp_surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, _workspace_surface_rect.width, _workspace_surface_rect.height);
  cairo_t *tmp_ctx = cairo_create(tmp_surface);
  cairo_set_source_surface(tmp_ctx, _plot_surface, -_workspace_surface_rect.x, -_workspace_surface_rect.y);
  cairo_rectangle(tmp_ctx, 0.0, 0.0, _workspace_surface_rect.width, _workspace_surface_rect.height);
  cairo_fill(tmp_ctx);

  const double bin_width = (_workspace_surface_rect.width / settings->bins) * ((_data_max - _data_min) / settings->GetRange().RangeX());
  const double bin_step = data_step.x * (fabs(settings->GetRange().min_x) - fabs(_data_min));
  int element_counter = 0;
  for (const auto &element : _data) {
    double x_position = element_counter++ * bin_width + bin_step;
    double y_position = element * data_step.y + _workspace_surface_rect.height;
    cairo_set_source_rgb(tmp_ctx, _color.back().red, _color.back().green, _color.back().blue);
    DrawRectangle(tmp_ctx, x_position, _workspace_surface_rect.height, bin_width, -element * data_step.y);
    MarkKeyValues(tmp_ctx, x_position, y_position);
  }
  cairo_set_source_surface(_plot_ctx, tmp_surface, _workspace_surface_rect.x, _workspace_surface_rect.y);
  cairo_paint(_plot_ctx);
  cairo_destroy(tmp_ctx);
  cairo_surface_destroy(tmp_surface);
}

void HistImpl::MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position) {
  if (settings->key_values) {
    cairo_set_source_rgb(ctx, 0.0, 0.0, 0.0);
    cairo_set_dash(ctx, dash, dash_length, 1.0);
    DrawLine(ctx, x_position, y_position, _workspace_surface_rect.x, y_position, 1.0);
    cairo_set_dash(_plot_ctx, dash, 0.0, 0.0);
  }
}

void HistImpl::DrawMultiplier(int exponent, double x_position, double y_position) {
  std::ostringstream str;
  cairo_text_extents_t extents;
  cairo_text_extents_t extents_exp;
  str.precision(0);
  str.str("");
  str << "x10";
  cairo_text_extents(_plot_ctx, str.str().c_str(), &extents);
  DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  str.str("");
  str << exponent;
  cairo_set_font_size(_plot_ctx, 14.0);
  cairo_text_extents(_plot_ctx, str.str().c_str(), &extents_exp);
  x_position = x_position + (extents.width + extents_exp.width) * 0.5;
  y_position = y_position - 8;
  DrawAxisValues(_plot_ctx, str.str(), x_position, y_position);
  cairo_set_font_size(_plot_ctx, 20.0);
}

void HistImpl::PrepareColors() {
  _color.push_back(Color::Generate(0, 1));
}

void HistImpl::SetMinMaxRange() {
  if (!settings->IsRangeSet()) {
    Range<double> range;
    range.min_x = _data_min;
    range.max_x = _data_max;
    range.min_y = 0.0;
    range.max_y = *std::max_element(_data.begin(), _data.end());
    settings->SetRange(range, !settings->IsRangeSet());
  }
}

Step<double> HistImpl::SetStepFromRange() {
  Step<double> step;
  step.x = _workspace_surface_rect.width / settings->GetRange().RangeX();
  step.y = _workspace_surface_rect.height / settings->GetRange().RangeY();
  return step;
}

void HistImpl::ClearData() {
  _data.clear();
  _data_min = std::numeric_limits<double>::max();
  _data_max = std::numeric_limits<double>::min();
}

void HistImpl::SortData(const std::vector<double> &data) {
  auto data_copy = data;
  std::sort(data_copy.begin(), data_copy.end());
  double step = fabs(_data_max - _data_min) / settings->bins;
  size_t bin_counter = 1;
  _data.clear();
  _data.reserve(settings->bins);
  _data.emplace_back(0.0);
  for (const auto &element : data_copy) {
    if (element <= (_data_min + bin_counter * step)) {
      _data.back() += 1.0;
    } else {
      _data.emplace_back(0.0);
      ++bin_counter;
    }
  }
}

} //  namespace oplot