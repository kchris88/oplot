#include <pie_impl.h>

// System includes
#include <iostream>
#include <numeric>
#include <sstream>

// Oplot includes
#include <context_guard.h>
#include <legend.h>
#include <point2d.h>

namespace oplot {

PieImpl::PieImpl(const Size &plot_size)
  : PieCore(plot_size),
    _radius(300.0),
    _explosion_radius(30.0) {

}

void PieImpl::Draw(const std::vector<double> &data) {
  _color.clear();
  ClearImage();

  PrepareData(data);
  PrepareColors();

  ContextGuard plot_guard(_plot_surface, &_plot_ctx);

  DrawPie();
  DrawLabels();
  DrawLegend(_data.size(), Legend::kRectangleMarker);
}

void PieImpl::Release() {
  Clear();
  settings->Clear();

  ReleaseImage();
}

void PieImpl::Clear() {
  _color.clear();

  ClearData();
  ClearImage();
}

void PieImpl::DrawLabels() {
  std::ostringstream str;
  SetLabelDataPrecision(str);
  
  std::vector<std::string> labels;
  labels.reserve(_data.size());
  const double max_width = PrepareLabels(labels, str);

  const double radius = _radius + max_width * 0.75;
  double angle_start = settings->rotation;
  const Point2D<double> center(static_cast<double>(_plot_surface_size.width >> 1),
                               static_cast<double>(_plot_surface_size.height >> 1));
  const double angle_shift_step = 0.02; // experimental value
  double angle_shift;
  cairo_text_extents_t extents;
  double double_pi = 2 * M_PI;
  SetFont(_plot_ctx, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD, 20.0);

  for (size_t i = 0; i < _data.size(); ++i) {
    cairo_set_source_rgb(_plot_ctx, 1.0, 1.0, 1.0);
    double angle_half = M_PI * _data[i];
    double angle_stop = angle_start + angle_half;
    if (angle_stop < 0.0) {
      angle_stop += double_pi;
    } else if (angle_stop > double_pi) {
      angle_stop -= double_pi;
    }
    if (angle_stop >= M_PI && angle_stop < double_pi) {
      angle_shift = -angle_shift_step;
    } else {
      angle_shift = angle_shift_step;
    }
    //double text_distance = sin(0.5 * angle_stop);
    if (settings->explode.size() <= i || !settings->explode[i]) {
      cairo_arc(_plot_ctx, center.x, center.y, radius, angle_start, angle_stop + angle_shift);
    } else {
      cairo_arc(_plot_ctx, center.x, center.y, radius + 30.0, angle_start, angle_stop + angle_shift);
    }
    angle_start = angle_stop + angle_half;
    cairo_save(_plot_ctx);
    double x, y, dx, dy;
    cairo_get_current_point(_plot_ctx, &x, &y);
    cairo_text_extents(_plot_ctx, labels[i].c_str(), &extents);
    cairo_set_source_rgb(_plot_ctx, 0.0, 0.0, 0.0);

    if (angle_stop <= M_PI_2) {
      dx = (extents.width * angle_stop) / M_PI;
      dy = (extents.height * angle_stop) / M_PI + extents.height * 0.5;
    } else if (angle_stop <= M_PI) {
      dx = (extents.width * angle_stop) / M_PI;
      dy = -(extents.height * angle_stop) / M_PI + extents.height * 1.5;
    } else if (angle_stop <= (M_PI + M_PI_2)) {
      dx = -(extents.width * angle_stop) / M_PI + extents.width * 2.0;
      dy = -(extents.height * angle_stop) / M_PI + extents.height * 1.5;
    } else {
      dx = -(extents.width * angle_stop) / M_PI + extents.width * 2.0;
      dy = (extents.height * angle_stop) / M_PI - extents.height * 1.5;
    }
    DrawText(_plot_ctx, labels[i], x - dx, y + dy);
    cairo_restore(_plot_ctx);
  }
}

void PieImpl::DrawPie() {
  oplot::Point2D<double> center;
  center.x = static_cast<double>(_plot_surface_size.width >> 1);
  center.y = static_cast<double>(_plot_surface_size.height >> 1);
  double angle_start = settings->rotation;
  double angle_stop;

  oplot::Point2D<double> previous;
  cairo_arc(_plot_ctx, center.x, center.y, _radius, angle_start, angle_start);
  cairo_get_current_point(_plot_ctx, &previous.x, &previous.y);

  for (size_t i = 0; i < _data.size(); ++i) {
    angle_stop = angle_start + 2 * M_PI * _data[i];
    if (settings->explode.size() <= i || !settings->explode[i]) {
      cairo_set_source_rgb(_plot_ctx, _color[i].red, _color[i].green, _color[i].blue);
      DrawArc(_plot_ctx, center, previous, angle_start, angle_stop);
    } else {
      Point2D<double> shifted_center;
      CalculateShiftedCenter(_plot_ctx, center, shifted_center, angle_start, _data[i]);
      cairo_set_source_rgb(_plot_ctx, _color[i].red, _color[i].green, _color[i].blue);
      DrawArcExploded(_plot_ctx, center, shifted_center, previous, angle_start, angle_stop);
    }
    angle_start = angle_stop;
    cairo_fill(_plot_ctx);
    cairo_stroke(_plot_ctx);
  }
}

void PieImpl::ClearData() {
  _data.clear();
}

void PieImpl::PrepareColors() {
  for (size_t i = 0; i < _data.size(); ++i) {
    _color.push_back(std::move(Color::Generate(i, _data.size())));
  }
}

void PieImpl::PrepareData(const std::vector<double> &data) {
  _data.clear();
  _data.reserve(data.size());
  double sum = std::accumulate(data.begin(), data.end(), 0.0);
  for (const auto &element : data) {
    _data.emplace_back(element / sum);
  }
}

void PieImpl::CalculateShiftedCenter(cairo_t *ctx, const Point2D<double> &center, Point2D<double> &shifted_center,
                                 const double angle_start, const double pie_data) {
  cairo_set_source_rgb(ctx, 1.0, 1.0, 1.0);
  cairo_arc(ctx, center.x, center.y, _explosion_radius, angle_start, angle_start + M_PI * pie_data);
  cairo_get_current_point(ctx, &shifted_center.x, &shifted_center.y);
  cairo_stroke(ctx);
}

void PieImpl::DrawArc(cairo_t *ctx, const Point2D<double> &center, Point2D<double> &actual, 
                  const double angle_start, const double angle_stop) {
  Point2D<double> new_position;
  cairo_arc(ctx, center.x, center.y, _radius, angle_start, angle_stop);
  cairo_get_current_point(ctx, &new_position.x, &new_position.y);
  cairo_move_to(ctx, center.x, center.y);
  cairo_line_to(ctx, actual.x, actual.y);
  cairo_line_to(ctx, new_position.x, new_position.y);
  actual = std::move(new_position);
}

void PieImpl::DrawArcExploded(cairo_t *ctx, const Point2D<double> &center, const Point2D<double> &shifted_center, Point2D<double> &actual, 
                          const double angle_start, const double angle_stop) {
  Point2D<double> new_position;
  cairo_arc(ctx, shifted_center.x, shifted_center.y, _radius, angle_start, angle_stop);
  cairo_get_current_point(ctx, &new_position.x, &new_position.y);
  cairo_move_to(ctx, shifted_center.x, shifted_center.y);
  cairo_line_to(ctx, actual.x + shifted_center.x - center.x, actual.y + shifted_center.y - center.y);
  cairo_line_to(ctx, new_position.x, new_position.y);
  actual.x = new_position.x - (shifted_center.x - center.x);
  actual.y = new_position.y - (shifted_center.y - center.y);
}

void PieImpl::SetLabelDataPrecision(std::ostringstream &str) {
  str.setf(std::ios_base::fixed);
  double tmp;
  int scale = 100;
  int max_scale = 10000;
  for (const auto &element : _data) {
    while (scale < max_scale) {
      tmp = element * ((element < 0.0) ? -scale : scale);
      if ((tmp - static_cast<int>(tmp)) > 0.0) {
        scale *= 10;
      } else {
        break;
      }
    }
    if (scale == max_scale) {
      break;
    }
  }
  switch (scale) {
    case 100:   { str.precision(0); return; }
    case 1000:  { str.precision(1); return; }
    case 10000: { str.precision(2); return; }
    default: return;
  }
}

double PieImpl::PrepareLabels(std::vector<std::string> &labels, std::ostringstream &str) {
  cairo_text_extents_t extents;
  double max_width = 0.0;
  for (const auto &element : _data) {
    str << element * 100 << "%";
    labels.emplace_back(str.str());
    cairo_text_extents(_plot_ctx, str.str().c_str(), &extents);
    if (max_width < extents.width) {
      max_width = extents.width;
    }
    str.str("");
  }
  return max_width;
}

} //  namespace oplot