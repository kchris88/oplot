#include <pie_core.h>

namespace oplot {

const double PieCore::dash[] = {5.0, 5.0};
const int PieCore::dash_length = sizeof(dash) / sizeof(dash[0]);

PieCore::PieCore(const Size &plot_size)
  : PlotCore(plot_size),
    settings(std::make_shared<PieSettings>()) {

}

void PieCore::Show(int delay_ms) {
  _window->SetTitle(settings->title);
  _window->SetDelay(delay_ms);
  if (_plot_surface_with_legend) {
    _window->Show(&_plot_surface_with_legend);
  } else {
    _window->Show(&_plot_surface);
  }
}

void PieCore::QuickSave() {
  Save(settings->title);
}

} //  namespace oplot