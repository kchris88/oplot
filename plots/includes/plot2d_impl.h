#pragma once

// Oplot includes
#include <plot2d_core.h>

namespace oplot {

class Plot2DImpl : public Plot2DCore {
public:
  Plot2DImpl(const Size &plot_size = Size(1000, 1000));
  Plot2DImpl(const Plot2DImpl &other) = default;
  ~Plot2DImpl() = default;

  void Draw(const std::vector<double> &y_data, const std::vector<double> &x_data = std::vector<double>());
  void Draw(const std::vector<std::vector<double>> &y_data, const std::vector<std::vector<double>> &x_data = std::vector<std::vector<double>>());

  void Release() override;
  void Clear() override;

protected:
  void DrawAxis() override;
  void DrawAxisDescription() override;
  void DrawGrid() override;
  void DrawPlot(const Step<double> &data_step);
  void DrawMeasurePoints(const Step<double> &data_step);
  void DrawMultiplier(int exponent, double x_position, double y_position);
  void MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position) override;

  void PrepareColors() override;

  void SetMinMaxRange() override;

  void SetRangeFromXData(size_t data_index);
  void SetRangeFromYData(size_t data_index);
  Step<double> SetStepFromRange();

  void ClearData() override;

private:
  void DrawFromData();

  std::vector<std::vector<double>> _x_data;
  std::vector<std::vector<double>> _y_data;
};

} //  namespace oplot