#pragma once

// Oplot includes
#include <pie_core.h>
#include <point2d.h>

namespace oplot {

class PieImpl : public PieCore {
public:
  PieImpl(const Size &plot_size = Size(1000, 1000));
  PieImpl(const PieImpl &other) = default;
  ~PieImpl() = default;

  void Draw(const std::vector<double> &data);

  void Release() override;
  void Clear() override;

protected:
  void DrawLabels() override;
  void DrawPie();

  void ClearData() override;

  void PrepareColors() override;

private:
  void PrepareData(const std::vector<double> &data);
  void CalculateShiftedCenter(cairo_t *ctx, const Point2D<double> &center, Point2D<double> &shifted_center,
                              const double angle_start, const double pie_data);
  void DrawArc(cairo_t *ctx, const Point2D<double> &center, Point2D<double> &actual, 
               const double angle_start, const double angle_stop);
  void DrawArcExploded(cairo_t *ctx, const Point2D<double> &center, const Point2D<double> &shifted_center, Point2D<double> &actual, 
                       const double angle_start, const double angle_stop);
  void SetLabelDataPrecision(std::ostringstream &str);
  double PrepareLabels(std::vector<std::string> &labels, std::ostringstream &str);

  std::vector<double> _data;
  double _radius;
  double _explosion_radius;
};

} //  namespace oplot