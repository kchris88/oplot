#pragma once

// Oplot includes
#include <plot_core.h>
#include <plot2d_settings.h>
#include <range.h>
#include <step.h>

namespace oplot {

class Plot2DCore : public PlotCore {
public:
  Plot2DCore(const Size &plot_size = Size());
  Plot2DCore(const Plot2DCore &other) = default;
  ~Plot2DCore() = default;

  void Show(int delay_ms = 0) override;
  void QuickSave();

  virtual void Release() = 0;
  virtual void Clear() = 0;

  std::shared_ptr<Plot2DSettings> settings;

protected:
  virtual void DrawAxis() = 0;
  virtual void DrawAxisDescription() = 0;
  virtual void DrawLabels() override;
  virtual void DrawGrid() = 0;
  virtual void MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position) = 0;
  virtual void DrawAxisValues(cairo_t *ctx, const std::string &str, const double x_position, const double y_position, bool horizontal_axis = true);
  virtual void DrawMultiplier(int exponent, double x_position, double y_position) = 0;

  virtual void PrepareColors() = 0;

  virtual void SetMinMaxRange() = 0;

  virtual void ClearData() = 0;

  static const double dash[];
  static const int dash_length;
};

} //  namespace oplot