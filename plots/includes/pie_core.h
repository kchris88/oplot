#pragma once

// Oplot includes
#include <pie_settings.h>
#include <plot_core.h>

namespace oplot {

class PieCore : public PlotCore {
public:
  PieCore(const Size &plot_size = Size());
  PieCore(const PieCore &other) = default;
  ~PieCore() = default;

  void Show(int delay_ms = 0) override;
  void QuickSave();

  virtual void Release() = 0;
  virtual void Clear() = 0;

  std::shared_ptr<PieSettings> settings;

protected:
  virtual void DrawLabels() = 0;
  virtual void ClearData() = 0;

  virtual void PrepareColors() = 0;

  static const double dash[];
  static const int dash_length;
};

} //  namespace oplot