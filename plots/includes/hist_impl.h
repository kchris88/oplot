#pragma once

// Oplot includes
#include <hist_core.h>

namespace oplot {

class HistImpl : public HistCore {
public:
  HistImpl(const Size &plot_size = Size(1000, 1000));
  HistImpl(const HistImpl &other) = default;
  ~HistImpl() = default;

  void Draw(const std::vector<double> &data);

  void Release() override;
  void Clear() override;

protected:
  void DrawAxis() override;
  void DrawAxisDescription() override;
  void DrawGrid() override;
  void DrawHist(const Step<double> &data_step);
  void MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position) override;
  void DrawMultiplier(int exponent, double x_position, double y_position);

  void PrepareColors() override;

  void SetMinMaxRange() override;

  Step<double> SetStepFromRange();

  void ClearData() override;

private:
  void SortData(const std::vector<double> &data);

  std::vector<double> _data;
  double _data_min;
  double _data_max;
};

} //  namespace oplot