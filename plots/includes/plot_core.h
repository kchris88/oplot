#pragma once

// System includes
#include <math.h>
#include <memory>
#include <string>
#include <vector>

// Cairo includes
#ifdef __CAIRO
#include <cairo/cairo.h>
#endif

// Oplot includes
#include <color.h>
#include <legend.h>
#include <rectangle.h>
#include <size.h>
#include <window.h>

#define _USE_MATH_DEFINES

namespace oplot {

class PlotCore {
public:
  PlotCore(const Size &plot_size = Size());
  PlotCore(const PlotCore &other) = default;
  ~PlotCore() = default;

  virtual void Show(int delay_ms = 0) = 0;
  void Save(const std::string &filename);
  Legend &legend();
  Legend legend() const;
  virtual void QuickSave() = 0;
  virtual void Release() = 0;
  virtual void Clear() = 0;

protected:
  virtual void ClearImage();
  virtual void ClearData() = 0;
  virtual void DrawLabels() = 0;
  void DrawLine(cairo_t *ctx, const double begin_x, const double begin_y, const double end_x, const double end_y, const double line_width);
  void DrawCircle(cairo_t *ctx, const double center_x, const double center_y, const double radius);
  void DrawRectangle(cairo_t *ctx, const double begin_x, const double begin_y, const double width, const double height);
  void DrawText(cairo_t *ctx, const std::string &text, const double begin_x, const double begin_y, const double angle = 0.0);
  void SetFont(cairo_t *ctx, const _cairo_font_slant slant, const _cairo_font_weight weight, const double size);

  int CalculateExponent(const double min, const double max);

  virtual void PrepareColors() = 0;

  virtual void ReleaseImage();

  void DrawLegend(const size_t plots_nbr, const size_t marker_type = Legend::kPointMarker);
  void PreparePlotSurfaceWithLegend(const int max_legend_width);

  cairo_surface_t *_plot_surface;
  cairo_surface_t *_plot_surface_with_legend;
  cairo_t *_plot_ctx;
  cairo_t *_plot_with_legend_ctx;
  Size _plot_surface_size;
  Size _plot_surface_with_legend_size;
  Rectangle _workspace_surface_rect;
  std::vector<Color> _color;
  Legend _legend;
  std::shared_ptr<Window> _window;
};

} //  namespace oplot