#pragma once

// Oplot includes
#include <bar_core.h>

namespace oplot {

class BarImpl : public BarCore {
public:
  BarImpl(const Size &plot_size = Size(1000, 1000));
  BarImpl(const BarImpl &other) = default;
  ~BarImpl() = default;

  void Draw(const std::vector<double> &data, const std::string &label);
  void Draw(const std::vector<std::vector<double>> &data, const std::vector<std::string> &labels);

  void Release() override;
  void Clear() override;

protected:
  void DrawAxis() override;
  void DrawAxisDescription() override;
  void DrawGrid() override;
  void DrawBar(const double step_y);
  void MarkKeyValues(cairo_t *ctx, const double x_position, const double y_position) override;
  void DrawMultiplier(int exponent, double x_position, double y_position);

  void PrepareColors() override;

  void SetMinMaxRange() override;

  Step<double> SetStepFromRange();

  void ClearData() override;

private:
  void DrawFromData();

  std::vector<std::vector<double>> _data;
  std::vector<std::string> _labels;
};

} //  namespace oplot