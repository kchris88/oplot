#pragma once

#include <memory>

// Oplot includes
#include <bar_impl.h>
#include <hist_impl.h>
#include <pie_impl.h>
#include <plot2d_impl.h>

namespace oplot {

/**
 * @brief Bar plot
 */
class Bar {
public:
  Bar(const Size &plot_size = Size(1000, 1000));
  Bar(const Bar &other) = default;
  ~Bar() = default;

  /**
   * @brief Function draw bar plot based on arguments and data from the past.
   * @param data - vector with data
   * @param label - label for data argument
   */
  void Draw(const std::vector<double> &data, const std::string &label);
  /**
   * @brief Function draw bar plot based on arguments and data from the past.
   * @param data - 2d vector with data
   * @param labels - vector of labels for data argument
   */
  void Draw(const std::vector<std::vector<double>> &data, const std::vector<std::string> &labels);

  /**
   * @brief Shows a window with a drawn graph.
   * @param delay_ms - this parameter determines a time that the further processing will be stopped.
   */
  void Show(int delay_ms = 0);
  /**
   * @brief Saves drawn plot to the file.
   * @param filename - path with filename where image will be saved,
   */
  void Save(const std::string &filename);
  /**
   * @brief Access to legend parameter.
   * @return Reference to legend parameter.
   */
  Legend &legend();
  /**
   * @brief Access to legend parameter.
   * @return copy of legend parameter.
   */
  Legend legend() const;
  /**
   * @brief Release entire object (destroys surfaces and data).
   */
  void Release();
  /**
   * @brief Clear data and surfaces to prepare object for new data.
   */
  void Clear();

  std::shared_ptr<BarSettings> settings;

private:
  std::unique_ptr<BarImpl> _bar_impl;
};

/**
 * @brief Histogram
 */
class Hist {
public:
  Hist(const Size &plot_size = Size(1000, 1000));
  Hist(const Hist &other) = default;
  ~Hist() = default;

  /**
   * @brief Function draw histogram based on arguments and data from the past.
   * @param data - vector with data.
   */
  void Draw(const std::vector<double> &data);

  /**
   * @brief Shows a window with a drawn graph.
   * @param delay_ms - this parameter determines a time that the further processing will be stopped.
   */
  void Show(int delay_ms = 0);
  /**
   * @brief Saves drawn plot to the file.
   * @param filename - path with filename where image will be saved,
   */
  void Save(const std::string &filename);
  /**
   * @brief Access to legend parameter.
   * @return Reference to legend parameter.
   */
  Legend &legend();
  /**
   * @brief Access to legend parameter.
   * @return copy of legend parameter.
   */
  Legend legend() const;
  /**
   * @brief Release entire object (destroys surfaces and data).
   */
  void Release();
  /**
   * @brief Clear data and surfaces to prepare object for new data.
   */
  void Clear();

  std::shared_ptr<HistSettings> settings;

private:
  std::unique_ptr<HistImpl> _hist_impl;
};

/**
 * @brief Pie chart
 */
class Pie {
public:
  Pie(const Size &plot_size = Size(1000, 1000));
  Pie(const Pie &other) = default;
  ~Pie() = default;

  /**
   * @brief Function draw pie chart based on arguments and data from the past.
   * @param data - vector with data
   */
  void Draw(const std::vector<double> &data);

  /**
   * @brief Shows a window with a drawn graph.
   * @param delay_ms - this parameter determines a time that the further processing will be stopped.
   */
  void Show(int delay_ms = 0);
  /**
   * @brief Saves drawn plot to the file.
   * @param filename - path with filename where image will be saved,
   */
  void Save(const std::string &filename);
  /**
   * @brief Access to legend parameter.
   * @return Reference to legend parameter.
   */
  Legend &legend();
  /**
   * @brief Access to legend parameter.
   * @return copy of legend parameter.
   */
  Legend legend() const;
  /**
   * @brief Release entire object (destroys surfaces and data).
   */
  void Release();
  /**
   * @brief Clear data and surfaces to prepare object for new data.
   */
  void Clear();

  std::shared_ptr<PieSettings> settings;

private:
  std::unique_ptr<PieImpl> _pie_impl;
};

/**
 * @brief 2D plot
 */
class Plot2D {
public:
  Plot2D(const Size &plot_size = Size(1000, 1000));
  Plot2D(const Plot2D &other) = default;
  ~Plot2D() = default;

  /**
   * @brief Function draw 2D plot based on arguments and data from the past.
   * @param y_data - plot's values vector
   * @param x_data - plot's arguments vector (optional)
   */
  void Draw(const std::vector<double> &y_data, const std::vector<double> &x_data = std::vector<double>());
  /**
   * @brief Function draw 2D plot based on arguments and data from the past.
   * @param y_data - 2d vector with plot's values
   * @param x_data - 2d vector with plot's arguments (optional)
   */
  void Draw(const std::vector<std::vector<double>> &y_data, const std::vector<std::vector<double>> &x_data = std::vector<std::vector<double>>());

  /**
   * @brief Shows a window with a drawn graph.
   * @param delay_ms - this parameter determines a time that the further processing will be stopped.
   */
  void Show(int delay_ms = 0);
  /**
   * @brief Saves drawn plot to the file.
   * @param filename - path with filename where image will be saved,
   */
  void Save(const std::string &filename);
  /**
   * @brief Access to legend parameter.
   * @return Reference to legend parameter.
   */
  Legend &legend();
  /**
   * @brief Access to legend parameter.
   * @return copy of legend parameter.
   */
  Legend legend() const;
  /**
   * @brief Release entire object (destroys surfaces and data).
   */
  void Release();
  /**
   * @brief Clear data and surfaces to prepare object for new data.
   */
  void Clear();

  std::shared_ptr<Plot2DSettings> settings;

private:
  std::unique_ptr<Plot2DImpl> _plot2d_impl;
};

} //  namespace oplot