#include <oplot.h>

namespace oplot {

Bar::Bar(const Size &plot_size)
  : settings(std::make_shared<BarSettings>()),
    _bar_impl(std::make_unique<BarImpl>(plot_size)) {
  _bar_impl->settings = settings;
}

void Bar::Draw(const std::vector<double> &data, const std::string &label) {
  _bar_impl->Draw(data, label);
}

void Bar::Draw(const std::vector<std::vector<double>> &data, const std::vector<std::string> &labels) {
  _bar_impl->Draw(data, labels);
}

void Bar::Show(const int delay_ms) {
  _bar_impl->Show(delay_ms);
}

void Bar::Save(const std::string &filename) {
  _bar_impl->Save(filename);
}

Legend &Bar::legend() {
  return _bar_impl->legend();
}

Legend Bar::legend() const {
  return _bar_impl->legend();
}

void Bar::Release() {
  settings->Clear();
  _bar_impl->Release();
}

void Bar::Clear() {
  settings->Clear();
  _bar_impl->Clear();
}

Hist::Hist(const Size &plot_size)
  : settings(std::make_shared<HistSettings>()),
    _hist_impl(std::make_unique<HistImpl>(plot_size)) {
  _hist_impl->settings = settings;
}

void Hist::Draw(const std::vector<double> &data) {
  _hist_impl->Draw(data);
}

void Hist::Show(int delay_ms) {
  _hist_impl->Show(delay_ms);
}

void Hist::Save(const std::string &filename) {
  _hist_impl->Save(filename);
}

Legend &Hist::legend() {
  return _hist_impl->legend();
}

Legend Hist::legend() const {
  return _hist_impl->legend();
}

void Hist::Release() {
  settings->Clear();
  _hist_impl->Release();
}

void Hist::Clear() {
  settings->Clear();
  _hist_impl->Clear();
}

Pie::Pie(const Size &plot_size)
  : settings(std::make_shared<PieSettings>()),
    _pie_impl(std::make_unique<PieImpl>(plot_size)) {
  _pie_impl->settings = settings;
}

void Pie::Draw(const std::vector<double> &data) {
  _pie_impl->Draw(data);
}

void Pie::Show(int delay_ms) {
  _pie_impl->Show(delay_ms);
}

void Pie::Save(const std::string &filename) {
  _pie_impl->Save(filename);
}

Legend &Pie::legend() {
  return _pie_impl->legend();
}

Legend Pie::legend() const {
  return _pie_impl->legend();
}

void Pie::Release() {
  settings->Clear();
  _pie_impl->Release();
}

void Pie::Clear() {
  settings->Clear();
  _pie_impl->Clear();
}

Plot2D::Plot2D(const Size &plot_size)
  : settings(std::make_shared<Plot2DSettings>()),
    _plot2d_impl(std::make_unique<Plot2DImpl>(plot_size)) {
  _plot2d_impl->settings = settings;
}

void Plot2D::Draw(const std::vector<double> &y_data, const std::vector<double> &x_data) {
  _plot2d_impl->Draw(y_data, x_data);
}
void Plot2D::Draw(const std::vector<std::vector<double>> &y_data, const std::vector<std::vector<double>> &x_data) {
  _plot2d_impl->Draw(y_data, x_data);
}

void Plot2D::Show(int delay_ms) {
  _plot2d_impl->Show(delay_ms);
}

void Plot2D::Save(const std::string &filename) {
  _plot2d_impl->Save(filename);
}

Legend &Plot2D::legend() {
  return _plot2d_impl->legend();
}

Legend Plot2D::legend() const {
  return _plot2d_impl->legend();
}

void Plot2D::Release() {
  settings->Clear();
  _plot2d_impl->Release();
}

void Plot2D::Clear() {
  settings->Clear();
  _plot2d_impl->Clear();
}

} //  namespace oplot