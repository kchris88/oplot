﻿# Oplot  

Open plot library was created as an alternative for existing solutions. Oplot library is very easy to use in your C++ projects.
This library will continue to be developed.

## Getting started  

To make installation as simple as possible I have already prepared a script that you can call to install Oplot library in you system (script tested only on Ubuntu 16.04).

```
cd oplot
sudo chmod +x oplot_install.sh
./oplot_install.sh
```

If you want to install the library by yourself you can use the instruction below.

### Prerequisits  

To use this library you will need CMake, Cairo and SDL2. To install this software you have to call the commands listed below.

```
sudo apt-cache search libsdl2
sudo apt install libcairo2-dev libsdl2-dev cmake
```

### Installing  

First of all you have to download zip/tar package or clone Oplot repository:
```
git clone https://gitlab.com/kchris88/oplot.git
```

Now you can install Oplot library in your system.

Create `build` folder in `oplot` directory:
```
mkdir build
cd build
```

Now you have to build the project:
```
cmake ..
```

Next step is installation:
```
sudo make install
```

Last step is creating the configuration file for Oplot library:
```
sudo /bin/bash -c 'echo "/usr/local/lib/oplot" > /etc/ld.so.conf.d/oplot.conf'
sudo ldconfig
```

Now you can remove oplot directory. The library is already installed in your system.

## Using (Ubuntu 16.04)  

To use Oplot library in your project you have to add includes and libraries directories:
```
-I/usr/local/include/oplot
-I/usr/include/x86_64-linux-gnu
-L/usr/local/lib/oplot
-L/usr/lib/x86_64-linux-gnu
```

and libraries flags:
```
-loplot -lcairo -lSDL2
```

Oplot library needs at least C++14 standard.

## Examples  

If you are looking how to use Oplot look at the example implementations and results in `example` directory.

## Issues

If you find any issue with this library or if you have any sugestion please let me know via email (krzysztof.kowalak@gmail.com).

## Author  

* **Krzysztof Kowalak** - *Initial work* - [kchris88](https://gitlab.com/kchris88)  

## License  

This software is redistributed on license BSD License 2.0.  

```
Copyright (c) 2017-2018, Krzysztof Kowalak <krzysztof.kowalak@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Krzysztof Kowalak nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL KRZYSZTOF KOWALAK BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
